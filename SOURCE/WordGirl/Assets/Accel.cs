using UnityEngine;
using System.Collections;

public class Accel : MonoBehaviour {
	
	//declare matrix and calibration vector
	private Matrix4x4 calibrationMatrix;

	private void OnGUI()
	{
		Vector3 accel = getAccelerometer(Input.acceleration);

		GUILayout.Button("X: " + accel.x.ToString("F2"));
		GUILayout.Button("Y: " + accel.y.ToString("F2"));
		GUILayout.Button("Z: " + accel.z.ToString("F2"));
	}

	//calibrate on Start and Mouse/Touch down
	private void Start()
	{
		CalibrateAccelerometer();
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0)) CalibrateAccelerometer();

		Vector3 accel = getAccelerometer(Input.acceleration);

		transform.position = Vector3.Lerp(transform.position, Vector3.up * accel.y * 5, Time.deltaTime * 5);
	}

	//Method for calibration 
	private void CalibrateAccelerometer()
	{
		Vector3 currentAcceleration = Input.acceleration;

		Quaternion rotateQuaternion = Quaternion.FromToRotation(new Vector3(0f, 0f, -1f), currentAcceleration);
		//create identity matrix ... rotate our matrix to match up with down vec
		Matrix4x4 matrix = Matrix4x4.TRS(Vector3.zero, rotateQuaternion, new Vector3(1f, 1f, 1f));
		//get the inverse of the matrix
		calibrationMatrix = matrix.inverse;
	}

	//Method to get the calibrated input 
	Vector3 getAccelerometer(Vector3 accelerator){
		Vector3 accel = this.calibrationMatrix.MultiplyVector(accelerator);
		return accel;
	}
}
