﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ParentGate : MonoBehaviour
{
	public static bool gateOpen = false;
	private bool isLoading = false;
	private bool verificationFailed = false;
	private bool isValidDay = false;
	private bool isValidMonth = false;
	private bool isValidYear = false;
	public string gameName;		/// store this from the LevelSelectionPanelScript so we known what to open to after successfully passing the ParentGate question

	public InputField monthInput;
	public InputField dayInput;
	public InputField yearInput;

	private tk2dUIItem closeButton;
	private tk2dUIItem submitButton;
	private string titleTextDefault = "How Old Are You?";

	///===============================

	void Awake()
	{
		closeButton = transform.Find( "CloseButton" ).GetComponent<tk2dUIItem>();
		closeButton.OnClickUIItem += OnClick;

		submitButton = transform.Find( "SubmitButton" ).GetComponent<tk2dUIItem>();
		submitButton.OnClickUIItem += OnClick;
	}

	void Open()
	{
		gameObject.SetActive( true );

		monthInput.text = "";
		dayInput.text = "";
		yearInput.text = "";
	}

	void Close()
	{
		gameObject.SetActive( false );
		GameObject.Find("Title").GetComponent<Title>().isLoading = false;
	}

	bool CheckDate()
	{
		System.DateTime birthday = new System.DateTime();

		int age;

		System.DateTime.TryParse( monthInput.text + "/" + dayInput.text + "/" + yearInput.text, out birthday );
		age = (int) (System.DateTime.Today - birthday).TotalDays/365;

		return age >= 13;
	}

	private void OnClick( tk2dUIItem uiItem )
	{
		isLoading = true;
		switch( uiItem.name )
		{			
			case "SubmitButton":
				if(isValidDay == false || isValidMonth == false || isValidYear == false)
				{
					string dateType = "";
					if(isValidMonth == false)
						dateType += "-month ";
					if(isValidDay == false)
						dateType += "-day ";
					if(isValidYear == false)
						dateType += "-year ";
					
					this.transform.FindChild("TitleBar/Text").GetComponent<tk2dTextMesh>().text = "Invalid " + dateType + " entered.";
				}
				else if( CheckDate() )
				{
					verificationFailed = false;
					LoadFromWeb();
					Close();
				}
				else if(verificationFailed == true)
				{
					Close();
				}
				else
				{
					verificationFailed = true;
					SetNotification();
				}
				break;
			
			case "CloseButton":
				Close();
				break;
		}
	}

	void SetNotification()
	{
		monthInput.gameObject.SetActive(false);
		dayInput.gameObject.SetActive(false);
		yearInput.gameObject.SetActive(false);

		this.transform.FindChild("SubmitButton/Text").GetComponent<tk2dTextMesh>().text = "OK";
		this.transform.FindChild("TitleBar/Text").GetComponent<tk2dTextMesh>().text = "You must be at least \n13 to unlock this content!";
	}

	/// <summary>
	/// Loads from file.
	/// </summary>
	void LoadFromWeb() 
	{
		UniWebView webView = CreateWebView();
		webView.OnLoadComplete += OnLoadComplete;
		webView.openLinksInExternalBrowser = true;
		webView.url = "http://livestage.curiousmedia.com/public/sch/more-apps";
		webView.Load();
	}

	void OnLoadComplete(UniWebView webView, bool success, string errorMessage) {
		if (success) 
		{
			webView.Show();
		} 
		else 
		{
			Debug.Log("Something wrong in webview loading: " + errorMessage);
		}
		isLoading = false;
		webView.OnLoadComplete -= OnLoadComplete;
	}

	/// <summary>
	/// Creates the Uni Web View Object.
	/// </summary>
	UniWebView CreateWebView() 
	{
		GameObject webViewGameObject = GameObject.Find("WebView");
		if (webViewGameObject == null) 
		{
			webViewGameObject = new GameObject("WebView");
		}

		UniWebView webView = webViewGameObject.AddComponent<UniWebView>();

		webView.toolBarShow = true;
		return webView;
	}

	public void SetNextInput(InputField txtInput)
	{
		if(txtInput.name == monthInput.name)
		{
			EventSystem.current.SetSelectedGameObject(dayInput.gameObject);

			isValidMonth = true;
		}
		else if(txtInput.name == dayInput.name)
		{
			EventSystem.current.SetSelectedGameObject(yearInput.gameObject);

			isValidDay = true;
		}
		else if(txtInput.name == yearInput.name)
		{
			isValidYear = true;
		}
	}

	public void SetInvalidDateEntry(InputField inputField, bool isValid)
	{
		if(inputField.name == monthInput.name)
		{
			isValidMonth = isValid;
		}
		else if(inputField.name == dayInput.name)
		{
			isValidDay = isValid;
		}
		else if(inputField.name == yearInput.name)
		{
			isValidYear = isValid;
		}

		if(isValid)
			this.transform.FindChild("TitleBar/Text").GetComponent<tk2dTextMesh>().text = titleTextDefault;
		else
			this.transform.FindChild("TitleBar/Text").GetComponent<tk2dTextMesh>().text = "Invalid " + inputField.name + " entered.";
	}


	public void OnTextChange( InputField inputField )
	{
		string newString = inputField.text;

		if(newString.Length > 0)
		{
			int dateEntry = int.Parse(newString);
			if(dateEntry > 0)
			{
				if(inputField.name.Contains("Month") && (dateEntry < 1 || dateEntry > 12))
				{
					SetInvalidDateEntry(inputField, false);
				}
				else if(inputField.name.Contains("Month"))
				{
					SetInvalidDateEntry(inputField, true);
					if(newString.Length >= inputField.characterLimit)
					{
						SetNextInput(inputField);
					}
				}

				if(inputField.name.Contains("Day") && (dateEntry < 1 || dateEntry > 31))
				{
					SetInvalidDateEntry(inputField, false);
				}
				else if(inputField.name.Contains("Day"))
				{
					SetInvalidDateEntry(inputField, true);
					if(newString.Length >= inputField.characterLimit)
					{
						SetNextInput(inputField);
					}
				}
					
				if(inputField.name.Contains("Year") && (dateEntry < 1800 || dateEntry > System.DateTime.Now.Year))
				{
					SetInvalidDateEntry(inputField, false);
				}
				else if(inputField.name.Contains("Year"))
				{
					SetInvalidDateEntry(inputField, true);
					if(newString.Length >= inputField.characterLimit)
					{
						SetNextInput(inputField);
					}
				}
			}
		}
	}
}