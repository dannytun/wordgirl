﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BaseButton : tk2dUIBaseItemControl
{
	public GameObject downObj;		/// object to animate when pressed
	public Vector3 downOffset = new Vector3( 0.05f, -0.05f, 0 );
	Vector3 upPos;
	string upIconName;		/// icon sprite name before it's pressed

	public Color downColor;
	Color upColor;
	string upButtonName;	/// button sprite name before it's pressed

	private AudioSource	audioSource;
	public AudioClip buttonSound, releaseSound;
	public bool defaultCharacterSounds = true;

	public GameObject targetObject;
	public string targetMessage = "Open";
	
	bool _disabled;
	public bool disabled
	{
		get { return _disabled; }
		
		set
		{
			_disabled = value;
			Color color = Color.white;
			if( _disabled )
				color = Color.gray;
			
			enabled = !_disabled;
			gameObject.GetComponent<tk2dUIItem>().enabled = !_disabled;
		}
	}
	
	/// sprites
	tk2dSprite sprite;
	tk2dSlicedSprite slicedSprite;
	tk2dTextMesh textMesh;

	void Awake()
	{
		//audioSource = (AudioSource) gameObject.AddComponent( typeof(AudioSource) );
		audioSource = GetComponent<AudioSource>();
		
		/// if not assigned, use first child as downObj
		if( downObj == null )
		{
			Transform child = transform.GetChild( 0 );
			if( child != null ) downObj = child.gameObject;
		}

		if( targetObject == null )
			targetObject = transform.parent.gameObject;

		/// don't get the UI Item from the editor - just set it to ourself!
		if( uiItem == null )
			uiItem = GetComponent<tk2dUIItem>();
		
		slicedSprite = GetComponent<tk2dSlicedSprite>();
		if( downObj != null )
			textMesh = downObj.GetComponent<tk2dTextMesh>();
	}
	
	void OnEnable()
	{
		if( uiItem )
		{
			uiItem.OnDown += ButtonDown;
			uiItem.OnUp += ButtonUp;
			uiItem.OnClick += ButtonClick;
		}
	}
	
	void OnDisable()
	{
		if( uiItem )
		{
			uiItem.OnDown -= ButtonDown;
			uiItem.OnUp -= ButtonUp;
			uiItem.OnClick -= ButtonClick;
		}
	}

	public void	PlaySound()
	{
		/// try attached sound clip
		if( audioSource != null )
			audioSource.Play();
		else
		{
			/// from SpaceStationInterior scripts for selecting character
			GameObject obj = GameObject.Find( "VoiceManager" );
			if( obj == null ) return;
			/*VoiceManager voiceManager = obj.GetComponent<VoiceManager>();
			if( voiceManager != null && voiceManager.IsQueueEmpty() )
			{
				///__MOBILE__ play the sound only if we can play the voice, too (not already selected)
				VoiceManager vm = StartGameScript.GetAFreeSFXManager();
				if( vm )
					vm.AddToQueue( (int)StartGameScript.SFX_Select.SFX_SELECT_ANY_CHARACTER, false );
			}*/
		}
	}
	
	private void ButtonDown()
	{
		PlaySound();

		if( downObj != null )
		{
			upPos = downObj.transform.localPosition;
			downObj.transform.Translate( downOffset );

			/// try to find a "-down" sprite
			tk2dSprite iconSprite = downObj.GetComponent<tk2dSprite>();
			if( iconSprite != null )
			{
				upIconName = iconSprite.GetCurrentSpriteDef().name;

				if( iconSprite.Collection.GetSpriteIdByName( upIconName+"-Down", -1 ) != -1 )
				//if( iconSprite.HasSprite( upIconName+"-Down" ) )
					iconSprite.SetSprite( upIconName+"-Down" );
			}
		}

		if( slicedSprite != null )
		{
			upButtonName = slicedSprite.GetCurrentSpriteDef().name;
			slicedSprite.SetSprite( upButtonName+"-Down" );
		}

		if( textMesh != null )
		{
			upColor = textMesh.color;
			textMesh.color = downColor;
		}
	}
	
	private void ButtonUp()
	{
		if( downObj != null )
		{
			downObj.transform.localPosition = upPos;

			/// restore icon
			tk2dSprite iconSprite = downObj.GetComponent<tk2dSprite>();
			if( iconSprite != null )
			{
				if( iconSprite.Collection.GetSpriteIdByName( upIconName, -1 ) != -1 )
				//if( iconSprite.HasSprite( upIconName ) )
					iconSprite.SetSprite( upIconName );
			}
		}

		if( slicedSprite != null )
		{
			slicedSprite.SetSprite( upButtonName );
		}

		if( textMesh != null )
		{
			textMesh.color = upColor;
		}
	}

	private void ButtonClick()
	{
		if( targetObject != null && !string.IsNullOrEmpty( targetMessage ) )
		{
			targetObject.SetActive( true );
			targetObject.SendMessage( targetMessage );
		}
	}
}
