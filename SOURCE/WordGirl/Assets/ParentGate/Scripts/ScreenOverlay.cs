﻿using UnityEngine;
using System.Collections;

public class ScreenOverlay : MonoBehaviour
{
	public bool tapToClose = true;
	public GameObject parentObject;

	void Awake()
	{
		tk2dSlicedSprite slicedSprite = GetComponent<tk2dSlicedSprite>();
		BoxCollider2D box = GetComponent<BoxCollider2D>();
		Vector2 worldSize = Camera.main.ViewportToWorldPoint( new Vector2( 1, 1 ) )*2;
		
		slicedSprite.dimensions = worldSize*100;
		box.size = worldSize;

		/// parent object
		if( parentObject == null )
			parentObject = transform.parent.gameObject;
		
		/// click events
		tk2dUIItem uiItem = GetComponent<tk2dUIItem>();
		if( uiItem != null )
			uiItem.OnDown += ButtonDown;
	}

	void ButtonDown()
	{
		if( tapToClose )
			parentObject.SendMessage( "Close" );
	}
}
