﻿using UnityEngine;
using System.Collections;

public static class CuriousUtilities {

	public static void SetPosX ( this Transform t, float value )
	{
		t.position = new Vector3(value, t.position.y, t.position.z);
	}

	public static void SetPosY ( this Transform t, float value )
	{
		t.position = new Vector3(t.position.x, value, t.position.z);
	}

	public static void SetPosZ ( this Transform t, float value )
	{
		t.position = new Vector3(t.position.x, t.position.y, value);
	}

	public static void SetLocalPosX ( this Transform t, float value, bool relative = false )
	{
		if (relative) {
			t.localPosition = new Vector3(t.localPosition.x + value, t.localPosition.y, t.localPosition.z);
		} else {
			t.localPosition = new Vector3(value, t.localPosition.y, t.localPosition.z);
		}
	}
	
	public static void SetLocalPosY ( this Transform t, float value )
	{
		t.localPosition = new Vector3(t.localPosition.x, value, t.localPosition.z);
	}
	
	public static void SetLocalPosZ ( this Transform t, float value )
	{
		t.localPosition = new Vector3(t.localPosition.x, t.localPosition.y, value);
	}

	public static void SetRotX ( this Transform t, float value )
	{
		t.rotation = Quaternion.Euler(value, t.rotation.eulerAngles.y, t.rotation.eulerAngles.z);
	}
	
	public static void SetRotY ( this Transform t, float value )
	{
		t.rotation = Quaternion.Euler(t.rotation.eulerAngles.x, value, t.rotation.eulerAngles.z);
	}
	
	public static void SetRotZ ( this Transform t, float value )
	{
		t.rotation = Quaternion.Euler(t.rotation.eulerAngles.x, t.rotation.eulerAngles.y, value);
	}

	public static void SetLocalRotX ( this Transform t, float value )
	{
		t.localRotation = Quaternion.Euler(value, t.localRotation.eulerAngles.y, t.localRotation.eulerAngles.z);
	}
	
	public static void SetLocalRotY ( this Transform t, float value )
	{
		t.localRotation = Quaternion.Euler(t.localRotation.eulerAngles.x, value, t.localRotation.eulerAngles.z);
	}
	
	public static void SetLocalRotZ ( this Transform t, float value )
	{
		t.localRotation = Quaternion.Euler(t.localRotation.eulerAngles.x, t.localRotation.eulerAngles.y, value);
	}

	public static void SetScale ( this Transform t, float value )
	{
		t.localScale = Vector3.one * value;
	}

	public static void SetScaleX ( this Transform t, float value )
	{
		t.localScale = new Vector3(value, t.localScale.y, t.localScale.z);
	}

	public static void SetScaleY ( this Transform t, float value )
	{
		t.localScale = new Vector3(t.localScale.x, value, t.localScale.z);
	}

	public static void SetScaleZ ( this Transform t, float value )
	{
		t.localScale = new Vector3(t.localScale.x, t.localScale.y, value);
	}

	public static void SetLayerRecursive( this GameObject obj, int layer )
	{
		obj.layer = layer;
		
		Transform[] children = obj.GetComponentsInChildren<Transform>(true);
		foreach (Transform child in children)
		{
			child.gameObject.layer = layer;
		}
	}
	
	public static Color GetRGBColor(float r, float g, float b, float a = 1) 
	{
		return new Color(r / 255f, g / 255f, b / 255f, a);
	}
	
}
