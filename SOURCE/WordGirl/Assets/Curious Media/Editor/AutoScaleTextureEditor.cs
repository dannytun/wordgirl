﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AutoScaleTexture))]
public class AutoScaleTextureEditor : Editor {
	
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		AutoScaleTexture myTarget = (AutoScaleTexture)target;

		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Parse Texture", GUILayout.Width(150))) {
			myTarget.ScaleTexture(myTarget.originalWidth, myTarget.originalHeight);
		}
		if (GUILayout.Button("Clear Texture", GUILayout.Width(150))) {
			myTarget.RevertTexture();
		}
		EditorGUILayout.EndHorizontal();
	}
}
