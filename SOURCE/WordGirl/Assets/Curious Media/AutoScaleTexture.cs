﻿using UnityEngine;
using System.Collections;

public class AutoScaleTexture : MonoBehaviour {

	public float originalWidth;
	public float originalHeight;
	public bool ScaleOnStart = true;

	void Start() {
		if (ScaleOnStart) {
			ScaleTexture(originalWidth, originalHeight);
		}
	}

	public void ScaleTexture (float oW, float oH) {
		tk2dSprite sprite = gameObject.GetComponent<tk2dSprite>();
		sprite.scale = new Vector3(1,1,1);
		float w = oW / sprite.GetBounds().size.x;
		float h = oH / sprite.GetBounds().size.y;
		sprite.scale = new Vector3(w,h,1);
	}

	public void RevertTexture () {
		tk2dSprite sprite = gameObject.GetComponent<tk2dSprite>();
		sprite.scale = new Vector3(1,1,1);
	}

}
