﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

/// <summary>
/// Handles loading basic UI Objects
/// </summary>

public static class ObjectLoader 
{
	/// <summary>
	/// Loads a background.
	/// </summary>
	public static GameObject LoadBackground(string bgName, string bgAsset, Vector3 bgPos, tk2dSprite.Anchor anchorPos, Vector2 textureScale )
	{
		GameObject bg = GameObject.Instantiate(Resources.Load("Objects/Prefabs/BG_Prefab")) as GameObject;
		bg.name = bgName;
		bg.transform.position = bgPos;
		tk2dSpriteFromTexture bgSprite = bg.GetComponent<tk2dSpriteFromTexture> ();

		string pathName = "Images/" + bgAsset;
		TextAsset ta = Resources.Load <TextAsset>(pathName);

		Texture2D texture2d = new Texture2D (1,1);
		texture2d.LoadImage (ta.bytes);
		texture2d.wrapMode = TextureWrapMode.Clamp;

		bgSprite.Create (tk2dSpriteCollectionSize.Default(), texture2d, anchorPos);
		bgSprite.spriteCollectionSize.pixelsPerMeter = 1f;
		bgSprite.ForceBuild ();
		bg.GetComponent<AutoScaleTexture> ().ScaleTexture (textureScale.x, textureScale.y);

		return bg;
	}

	/// <summary>
	/// Loads a buttons.
	/// </summary>
	public static GameObject LoadButton(string collectionData, string btnName, string btnAsset, Vector3 btnPos , Transform parentObjTrans = null)
	{
		//Play Button
		GameObject btn = GameObject.Instantiate(Resources.Load("Objects/Prefabs/ButtonsPrefab")) as GameObject;
		btn.name = btnName;
		btn.GetComponent<tk2dSprite> ().SetSprite((Resources.Load("Objects/Collections/" + collectionData) as GameObject).GetComponent<tk2dSpriteCollectionData>(), btnAsset);
		btn.transform.parent = parentObjTrans;
		btn.transform.localPosition = btnPos;

		BoxCollider2D btnCol = btn.GetComponent<BoxCollider2D>();
		btnCol.size = btn.GetComponent<MeshRenderer>().bounds.size;
		return btn;
	}

	/// <summary>
	/// Loads button smoke FX.
	/// </summary>
	public static GameObject LoadButtonFX(Vector3 btnPos, Transform parentObjTrans = null)
	{
		//Play Button
		GameObject btnFX = GameObject.Instantiate(Resources.Load("Objects/Prefabs/SmokeEffect_Prefab")) as GameObject;
		btnFX.name = "SmokeFX";
		btnFX.transform.localPosition = btnPos;
		btnFX.transform.parent = parentObjTrans;
		return btnFX;
	}

	/// <summary>
	/// Loads a Unity UI Button
	/// </summary>
	/// <returns>The canvas user interface button.</returns>
	public static GameObject LoadCanvasUIButton(string btnName, Vector3 pos, Vector2 size, Color color, string textureName = "", string canvasParent = "Canvas")
	{
		GameObject btnObj = new GameObject (btnName);
		btnObj.AddComponent<CanvasRenderer> ();
		btnObj.AddComponent<RawImage> ();
		btnObj.GetComponent<RawImage> ().color = color;

		if (textureName != "")
		{
			string pathName = "Images/" + textureName;
			TextAsset ta = Resources.Load <TextAsset>(pathName);

			Texture2D texture2d = new Texture2D (1,1);
			texture2d.LoadImage (ta.bytes);
			texture2d.wrapMode = TextureWrapMode.Clamp;

			btnObj.GetComponent<RawImage> ().texture = texture2d;
		}


		btnObj.AddComponent<Button> ();
		btnObj.GetComponent<Button> ().transition = Selectable.Transition.None;
		btnObj.transform.SetParent(GameObject.Find (canvasParent).transform);
		btnObj.transform.localScale = Vector3.one;
		btnObj.GetComponent<RectTransform>().anchoredPosition = pos;
		btnObj.GetComponent<RectTransform> ().sizeDelta = size;

		return btnObj;
	}

	/// <summary>
	/// Loads a Unity UI Button
	/// </summary>
	/// <returns>The canvas user interface button.</returns>
	public static GameObject LoadCanvasPopUp(string btnName, Vector3 pos, Vector2 size, Color color, string textureName = "", string canvasParent = "Canvas")
	{
		GameObject btnObj = GameObject.Instantiate(Resources.Load("Objects/Prefabs/PopUp_Prefab")) as GameObject;
		btnObj.name = btnName;

		if (textureName != "")
		{
			string pathName = "Images/" + textureName;
			TextAsset ta = Resources.Load <TextAsset>(pathName);

			Texture2D texture2d = new Texture2D (1,1);
			texture2d.LoadImage (ta.bytes);
			texture2d.wrapMode = TextureWrapMode.Clamp;

			btnObj.GetComponent<RawImage> ().texture = texture2d;
		}
			
		btnObj.transform.SetParent(GameObject.Find (canvasParent).transform);
		btnObj.transform.localScale = Vector3.one;
		btnObj.GetComponent<RectTransform>().anchoredPosition = pos;
		btnObj.GetComponent<RectTransform> ().sizeDelta = size;

		return btnObj;
	}
}
