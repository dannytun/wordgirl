﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

/// <summary>
/// Static data to load and parse json data files.
/// </summary>
public class StaticData
{
	public static List<string> wordsList= new List<string> ();
	public static string correctWord;
	public static string bgImagePrefix;
	public static List<AIType> aiTypes = new List<AIType> ();
	public static List<ComicStripPanelObjects> comicStripPanels = new List<ComicStripPanelObjects>();
	public static List<ComicStripPanelSubObjects> comicStripSubObjs = new List<ComicStripPanelSubObjects>();

	/// <summary>
	/// Loads the json level data.
	/// </summary>
	/// <param name="level">Level.</param>
	public static void LoadJsonLevelData(int level)
	{
		TextAsset jsonString = Resources.Load ("LevelData/level_"+level) as TextAsset;
		JSONNode node = JSON.Parse(jsonString.text);

		StaticData.bgImagePrefix = node ["backgroundImagePrefix"];

		//Create Word Dictionary
		StaticData.wordsList.Add (node ["word1"]);
		StaticData.wordsList.Add (node ["word2"]);
		StaticData.wordsList.Add (node ["word3"]);
		StaticData.correctWord = node ["correctWord"];

		for(int i = 0; i < node["panelPosArray"].Count; i++)
		{
			StaticData.ComicStripPanelObjects panelObjs = new StaticData.ComicStripPanelObjects ();
			panelObjs.anchor = (tk2dBaseSprite.Anchor)Enum.Parse(typeof(tk2dBaseSprite.Anchor), node ["panelPosArray"] [i] ["anchor"]);
			panelObjs.startingPos = new Vector3 (node ["panelPosArray"] [i] ["startX"].AsFloat, node ["panelPosArray"] [i] ["startY"].AsFloat, 0f);

			float desZ = 0f;;
			if(node ["panelPosArray"] [i] ["desZ"] != null)
				desZ = node ["panelPosArray"] [i] ["desZ"].AsFloat;
			
			panelObjs.desPos = new Vector3 (node ["panelPosArray"] [i] ["desX"].AsFloat, node ["panelPosArray"] [i] ["desY"].AsFloat, desZ);
			StaticData.comicStripPanels.Add (panelObjs);
		}

		for(int i = 0; i < node["subObjsArray"].Count; i++)
		{
			StaticData.ComicStripPanelSubObjects subObjs = new StaticData.ComicStripPanelSubObjects ();
			subObjs.panelIndexSubObjs = node["subObjsArray"] [i] ["panelIndexSubObjs"].AsInt;
			subObjs.subObjPos = new Vector3 (node ["subObjsArray"] [i] ["posX"].AsFloat, node ["subObjsArray"] [i] ["posY"].AsFloat, 0f);
			StaticData.comicStripSubObjs.Add (subObjs);
		}


		for (int i = 0; i < node ["levelObjectsArray"].Count; i++) 
		{
			// Build load function for placing ai and words.
			StaticData.AIType aiType = new StaticData.AIType ();
			aiType.aiType = node ["levelObjectsArray"] [i] ["AIType"];
			aiType.pos = new Vector2 ((node ["levelObjectsArray"] [i] ["intitialX"]).AsFloat, (node ["levelObjectsArray"] [i] ["intitialY"]).AsFloat);
			aiType.imageFileName =  node ["levelObjectsArray"][i]["imageFileName"];
			StaticData.aiTypes.Add (aiType);

		}

	}

	/// <summary>
	/// Clears the data.
	/// </summary>
	public static void ClearData()
	{
		aiTypes.Clear();
		wordsList.Clear();
		comicStripPanels.Clear ();
		comicStripSubObjs.Clear ();
		correctWord = string.Empty;
		bgImagePrefix = string.Empty;
	}

	/// <summary>
	/// AI Object Type Class Container.
	/// </summary>
	public class AIType
	{
		public string aiType;
		public Vector2 pos;
		public string imageFileName;
	}

	/// <summary>
	/// Comic Strip Panel Class Container
	/// </summary>
	public class ComicStripPanelObjects
	{
		public tk2dBaseSprite.Anchor anchor; 
		public Vector3 startingPos;
		public Vector3 desPos;
	}

	/// <summary>
	/// Comic Strip Panel Subobjects.
	/// </summary>
	public class ComicStripPanelSubObjects
	{
		public int panelIndexSubObjs;
		public Vector3 subObjPos; 
	}
}
	