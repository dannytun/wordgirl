using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {
	
	public static SoundManager Instance;
	
	public static Transform soundObject;
	
	public static List<string> music = new List<string>();
	public static List<string> sfx = new List<string>(); 
	public static List<string> vo = new List<string>(); 
	
	public static Dictionary<string, float> timePlaying = new Dictionary<string, float>();
	public static List<AudioSource> ambientSounds;

	private static float MusicVolume = 0.35f;
	
	private static float voiceoverGap = 0.5f;
	private float fadeSpeed = 0.001f;
	public static string currentMusic = "";
	public static bool isFading = false;

	public static bool musicMuted = false;
	public static bool sfxMuted = false;

	public static bool forceDuck = false;
	
	private static GameObject voiceover;
	private static AudioSource currentVoiceOver;
	
	private static List<string[]> voiceoverChain;
	private static bool isPlayingChain = false;
	private static bool isVoiceoverPlaying = false;
	
	public delegate void VOFinishCallback();
	public static VOFinishCallback OnVOFinishCallback { get; set; }
	
	public delegate void VOLoadFinishCallback();
	public static VOLoadFinishCallback OnVOLoadFinishCallback { get; set; }
	
	// Use this for initialization
	void Awake () {
		Instance = this;
		
		DontDestroyOnLoad(gameObject);
		
		gameObject.AddComponent<AudioListener>();

		soundObject = transform;
		
		voiceover = new GameObject("VoiceOver");
		voiceover.transform.parent = transform;
		
		Object[] storedMusic = Resources.LoadAll("Sounds/Music");
		foreach (Object txt in storedMusic) {
			music.Add(txt.name);
		}
		
		Object[] storedSFX = Resources.LoadAll("Sounds/SFX");
		foreach (Object txt in storedSFX) {
			sfx.Add(txt.name);
			timePlaying.Add(txt.name, 0);
		}
		
		foreach (string s in music) {
			GameObject sound = new GameObject(s);
			sound.transform.parent = transform;
			sound.AddComponent<AudioSource>();
			sound.GetComponent<AudioSource>().playOnAwake = false;
			sound.GetComponent<AudioSource>().loop = true;
			sound.GetComponent<AudioSource>().clip = Resources.Load("Sounds/Music/" + s) as AudioClip;
			sound.GetComponent<AudioSource>().volume = 0;
			sound.GetComponent<AudioSource>().priority = 126;
		}
		
		foreach (string s in sfx) {
			GameObject sound = new GameObject(s);
			sound.transform.parent = transform;
			sound.AddComponent<AudioSource>();
			sound.GetComponent<AudioSource>().playOnAwake = false;
			sound.GetComponent<AudioSource>().clip = Resources.Load("Sounds/SFX/" + s) as AudioClip;
			sound.GetComponent<AudioSource>().priority = 127;
		}
	}
	
	public static bool isLoopingSound(string name) {
		if (name != "") {
			return soundObject.FindChild(name).GetComponent<AudioSource>().loop;
		}
		return false;
	}

	public static bool isPlayingSound(string name) {
		if (name != "") {
			if (soundObject.FindChild(name).GetComponent<AudioSource>().loop == false) {
				if ((Time.time - timePlaying[name]) >= soundObject.FindChild(name).GetComponent<AudioSource>().clip.length) {
					return false;	
				}
				return true;
			} else {
				return soundObject.FindChild(name).GetComponent<AudioSource>().isPlaying;
			}
		}
		return false;
	}
	
	public static float getVolume(string name) {
		if (name != "") {
			return soundObject.FindChild(name).GetComponent<AudioSource>().volume;	
		}
		return 0;
	}
	
	public static void changeVolume(string name, float vol = 1.0f) {
		soundObject.FindChild(name).GetComponent<AudioSource>().volume = vol;
	}
	
	public static AudioSource PlaySound(string name, bool oneshot = true, bool loop = true, float delay = 0.0f, float volume = 1f, float pitch = 1f) {
		if (soundObject == null) { print ("Sound Manager not initialized: " + name); return null; }
		if (!soundObject.FindChild(name)) { print ("Cannot find sound: " + name); return null; }
		AudioSource audioSource = soundObject.FindChild(name).GetComponent<AudioSource>();	
		audioSource.spatialBlend = 0f;
		audioSource.pitch = pitch;
		
		if (oneshot) {
			if (delay > 0.0f) {
				audioSource.PlayDelayed(delay);
				timePlaying[name] = Time.time - delay;
			} else {
				audioSource.PlayOneShot(audioSource.clip, volume);
				timePlaying[name] = Time.time;
			}
		} else {
			if (isPlayingSound(name) == false) {
				audioSource.loop = loop;
				audioSource.PlayDelayed(delay);
			}
		}
		return audioSource;
	}
	
	public static void StopSound(string name) {
		if (soundObject == null) { print ("Sound Manager not initialized: " + name); return; }
		if (name != "") soundObject.FindChild(name).GetComponent<AudioSource>().Stop();
	}
	
	public static void muteSound(string name) {
		soundObject.FindChild(name).GetComponent<AudioSource>().mute = true;
	}
	
	public static void unmuteSound(string name) {
		soundObject.FindChild(name).GetComponent<AudioSource>().mute = false;	
	}

	public static void MuteSFX(bool mute) {
		sfxMuted = mute;

		foreach (string s in sfx) {
			soundObject.FindChild(s).GetComponent<AudioSource>().mute = sfxMuted;
		}	
	}

	public static void MuteMusic(bool mute) {
		musicMuted = mute;
		
		foreach (string s in music) {
			soundObject.FindChild(s).GetComponent<AudioSource>().mute = musicMuted;
		}	
	}
	
	public void PlayMusic(string name, bool dontFadeIn = false)
	{
		if (soundObject == null) { print ("Sound Manager not initialized: " + name); return; }
		if (!soundObject.FindChild(name)) { print ("Cannot find music: " + name); return; }
		
		AudioListener.volume = 1;
		
		StopAllCoroutines();
		StartCoroutine(xFadeMusic(name, dontFadeIn));
	}
	
	public IEnumerator xFadeMusic(string name, bool dontFadeIn = false) {
		//Check Playing
		if (currentMusic == name) {
			isFading = false;
			yield break;	
		}
		
		//Fade Out
		if (currentMusic != "") {
			while (getVolume(currentMusic) > 0) {
				changeVolume(currentMusic, getVolume(currentMusic) - fadeSpeed);	
				yield return null;
			}
			changeVolume(currentMusic, 0);
			StopSound(currentMusic);
		}
		
		//Fade In
		currentMusic = name;
		if (dontFadeIn == false) {
			PlaySound(currentMusic, false);
			while (getVolume(currentMusic) < MusicVolume) {
				changeVolume(currentMusic, getVolume(currentMusic) + fadeSpeed);	
				yield return null;
			}
			changeVolume(currentMusic, MusicVolume);	
		}
		
		isFading = false;
	}
	
	public IEnumerator lowerVolume(string name, float vol) {
		//Fade
		if (currentMusic != "") {
			while (getVolume(currentMusic) > vol) {
				changeVolume(currentMusic, getVolume(currentMusic) - fadeSpeed);	
				yield return null;
			}
			changeVolume(currentMusic, vol);
		}
	}
	
	public IEnumerator raiseVolume(string name, float vol) {
		//Fade
		if (currentMusic != "") {
			while (getVolume(currentMusic) < vol) {
				changeVolume(currentMusic, getVolume(currentMusic) + fadeSpeed);	
				yield return null;
			}
			changeVolume(currentMusic, vol);
		}
	}
	
	///////////////
	// VoiceOver //
	///////////////
	
	public static void LoadVoiceOver(string gameID)
	{
		if (voiceover.transform.Find(gameID) == null)
		{
			GameObject holder = new GameObject(gameID);
			holder.transform.parent = voiceover.transform;
			
			Object[] storedVO = Resources.LoadAll("Sounds/VoiceOver/" + gameID);
			foreach (Object sound in storedVO) {
				vo.Add(sound.name);
				string soundKey = "VoiceOver/" + gameID + "/" + sound.name;
				timePlaying.Add(soundKey, 0);
				
				string assetPath = "Sounds/VoiceOver/" + gameID + "/" + sound.name;
				
				//print("Loading: " + assetPath);
				GameObject newsound = new GameObject(sound.name);
				newsound.transform.parent = holder.transform;
				newsound.AddComponent<AudioSource>();
				newsound.GetComponent<AudioSource>().playOnAwake = false;
				newsound.GetComponent<AudioSource>().clip = Resources.Load(assetPath) as AudioClip;
			}
		}
		
		if (OnVOLoadFinishCallback != null) {
			OnVOLoadFinishCallback();	
		}
	}
	
	public static void LoadVoiceOverSpecific(string gameID, string gameIDLoad, string specificName)
	{
		if (voiceover.transform.Find(gameID) != null)
		{
			GameObject holder = voiceover.transform.Find(gameID).gameObject;

			Debug.LogError ("Load VO: " + "Sounds/VoiceOver/" + gameIDLoad + "/" + specificName);
			
			Object sound = Resources.Load("Sounds/VoiceOver/" + gameIDLoad + "/" + specificName);
			vo.Add(sound.name);
			string soundKey = "VoiceOver/" + gameID + "/" + sound.name;
			timePlaying.Add(soundKey, 0);
			
			string assetPath = "Sounds/VoiceOver/" + gameIDLoad + "/" + sound.name;
			
			print("Loading: " + assetPath);
			GameObject newsound = new GameObject(sound.name);
			newsound.transform.parent = holder.transform;
			newsound.AddComponent<AudioSource>();
			newsound.GetComponent<AudioSource>().playOnAwake = false;
			newsound.GetComponent<AudioSource>().clip = Resources.Load(assetPath) as AudioClip;
		}
	}
	
	public static void UnloadVoiceOver(string gameID)
	{
		Transform holder = voiceover.transform.FindChild(gameID);
		if (holder != null)
		{
			foreach (Transform t in holder) {
				vo.Remove(t.name);
				string soundKey = "VoiceOver/" + gameID + "/" + t.name;
				timePlaying.Remove(soundKey);
			}
			Destroy(holder.gameObject);
		}
		Resources.UnloadUnusedAssets();
	}
	
	public static AudioSource PlayVoiceOver(string name, string gameID, float delay = 0.0f, bool dontOverride = false, bool isChain = false)
	{		
		if (!isChain) {
			isVoiceoverPlaying = false;
		}
		
		if (dontOverride) {
			if (!isVoiceoverPlaying) {
				if (SoundManager.currentVoiceOver == null) {
					StopVoiceOver(true);
					currentVoiceOver = PlaySound("VoiceOver/" + gameID + "/" + name, true, false, delay);
				} else { 
					if (!isPlayingVoiceover(name, gameID)) {
						StopVoiceOver(true);
						currentVoiceOver = PlaySound("VoiceOver/" + gameID + "/" + name, true, false, delay);
					}
				}
			}
		} else {
			StopVoiceOver(true);
			currentVoiceOver = PlaySound("VoiceOver/" + gameID + "/" + name, true, false, delay);
		}
		
		return currentVoiceOver;
	}
	
	public static void StopVoiceOver(bool stopChain = false)
	{	
		if (currentVoiceOver != null) {
			currentVoiceOver.Stop();
		}
		currentVoiceOver = null;
		
		isPlayingChain = stopChain;
		if (!isPlayingChain) {
			voiceoverChain = null;
		}
	}
	
	public static bool isPlayingVoiceover(string name, string gameID) {
		if (name != "") {
			if (voiceover.transform.FindChild(gameID + "/" +name).GetComponent<AudioSource>().loop == false) {
				if ((Time.time - timePlaying["VoiceOver/" + gameID + "/" + name]) >= (voiceover.transform.FindChild(gameID + "/" + name).GetComponent<AudioSource>().clip.length) + voiceoverGap) {
					return false;	
				}
				return true;
			} else {
				return voiceover.transform.FindChild(gameID + "/" + name).GetComponent<AudioSource>().isPlaying;
			}
		}
		return false;
	}
	
	public static void PlayVoiceOverRandom(string[] name, string gameID, float delay = 0.0f, bool dontOverride = false) {
		PlayVoiceOver(name[Random.Range(0, name.Length)], gameID, delay, dontOverride);
	}
	
	public static void PlayVoiceOverChain(string[] name, string gameID, float delay = 0.0f, bool dontOverride = false) {
		isVoiceoverPlaying = true;
		isPlayingChain = false;
		voiceoverChain = new List<string[]>();
		for (int i = 0; i < name.Length; i++) {
			voiceoverChain.Add(new string[4]{
				name[i],
				gameID,
				delay.ToString(),
				dontOverride.ToString(),
			});
		}
	}
	
	void Update() {
		if (voiceoverChain != null) {
			if (voiceoverChain.Count > 0) {
				if (!isPlayingChain) {
					isPlayingChain = true;
					PlayVoiceOver(voiceoverChain[0][0], voiceoverChain[0][1], float.Parse(voiceoverChain[0][2]), bool.Parse(voiceoverChain[0][3]), true);
				} else if (!isPlayingVoiceover(voiceoverChain[0][0], voiceoverChain[0][1])) {
					voiceoverChain.RemoveAt(0);
					isPlayingChain = false;
					
					if (voiceoverChain.Count <= 0) {
						isVoiceoverPlaying = false;
						if (OnVOFinishCallback != null) {
							OnVOFinishCallback();	
						}	
					}
				}
			}
		}
	}
}