﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;
using DG.Tweening;


public class GamePlay : MonoBehaviour 
{

	private int playerPoints = 0;

	private float txScaleWidth = 2048f;
	private float txScaleHeight = 1536f;

	private float pauseScrollTime = 2f;

	private int panelCount = 0;

	const float constBGScrollSpeed = 0.1f;
	const float constObjScrollSpeed = 695f;

	private float playerMoveSpeed = 400.0f;
	private float playerTimeSpeed = 10.0f;
	private float scrollSpeed = 695f;

	private float bgScrollSpeed = 0.1f;
	private Vector2 uvOffset = Vector2.zero;
	private float modReset = 3.3f;

	private float waitTime;

	private Transform hero;
	private Matrix4x4 calibrationMatrix;

	private Vector3 minScreenBounds;
	private Vector3 maxScreenBounds;
	private Bounds heroBounds;

	private GameObject scrollBgRoot;

	private bool missionPlaying = false;
	private bool pauseScrollBG = false;
	private bool deviceDissabled = false;
	private bool resetObjects = false;

	private Transform[] bgTrans = new Transform[3];
	private List<GameObject> aiObjs = new List<GameObject>();

	private HUD hudScript;

	void OnEnable()
	{
		deviceDissabled = false;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	void OnDisable()
	{
		deviceDissabled = true;
		Screen.sleepTimeout = SleepTimeout.SystemSetting;
	}

	void Start () 
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		
		hudScript = GameObject.Find ("HUD").GetComponent<HUD> ();

		scrollBgRoot = new GameObject ("ScrollBGRoot");
		scrollBgRoot.transform.position = Vector3.zero;

		if(hero == null)
			hero = GameObject.Find ("Hero").transform;
		hero.GetComponent<tk2dSpriteAnimator> ().Play ();
		heroBounds= hero.GetComponent<tk2dSpriteAnimator> ().Sprite.GetBounds ();

		LoadBGAndPlace ();
		LoadLevelObjects ();

		LoadVOIntro ();

		LoadBackGroundMusic ();				
	}
				
	void Update()
	{
		if (missionPlaying == false && Input.anyKeyDown) //&& Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began)
		{
			SoundManager.StopVoiceOver (true);

			string[] startSounds = new string[]{ "WordUp" };
			SoundManager.PlayVoiceOverChain (startSounds, "Generic", 0, false);

			if(hero != null)
				hero.GetComponent<Player> ().isHeroWaiting = false;
			else
				hero = GameObject.Find ("Hero").transform;
				
			CalibrateAccelerometer();

			missionPlaying = true;
		}

		if(missionPlaying)
			MovePlayer ();		

		if (Input.GetKeyDown(KeyCode.Escape))
			ButtonBackToLvlSelection();
	}

	void LateUpdate() 
	{
		if(missionPlaying && deviceDissabled == false)
		{
			if (pauseScrollBG && waitTime > Time.time)
			{
				return;
			}
			else if (pauseScrollBG)
			{
				bgScrollSpeed += Mathf.Lerp (0, constBGScrollSpeed, Time.fixedDeltaTime);
				if ((bgScrollSpeed + .001f) > constBGScrollSpeed)
				{
					bgScrollSpeed = constBGScrollSpeed;
				}

				scrollSpeed += Mathf.Lerp (0, constObjScrollSpeed, Time.fixedDeltaTime);
				if ((scrollSpeed + 1f) > constObjScrollSpeed)
				{
					scrollSpeed = constObjScrollSpeed;
				}

				if ((scrollSpeed + .001f) > constObjScrollSpeed && (bgScrollSpeed + 1f) > constBGScrollSpeed)
				{
					pauseScrollBG = false;
				}
			}

			uvOffset += ( Vector2.right * Time.fixedDeltaTime * bgScrollSpeed);
			scrollBgRoot.transform.Translate(Vector2.left * Time.fixedDeltaTime * scrollSpeed);

			bgTrans [0].GetComponent<SpriteRenderer> ().material.SetTextureOffset ("_MainTex", uvOffset);

			if (resetObjects == false && (bgTrans [0].GetComponent<SpriteRenderer> ().material.mainTextureOffset.x % modReset) > 3.2f) //2.9f
			{
				resetObjects = true;
			}
			else if(resetObjects == true && (bgTrans [0].GetComponent<SpriteRenderer> ().material.mainTextureOffset.x % modReset) < 0.1f)
			{
				resetObjects = false;
				scrollBgRoot.transform.position = Vector2.right * txScaleWidth;
				ClearAndLoadObjects ();
			}
		}
	}
				

	/// <summary>
	/// Moves the player using the accelerometer.
	/// </summary>
	void MovePlayer()
	{
		Vector3 accel2 = getAccelerometer(Input.acceleration);

		minScreenBounds = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
		maxScreenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));

		Vector3 heroPos = hero.position;
		heroPos = Vector3.Lerp(heroPos, new Vector3(heroPos.x, heroPos.y + (accel2.y * playerMoveSpeed), heroPos.z), Time.deltaTime * playerTimeSpeed);

		Vector3 pos = heroPos;
		pos.x = Mathf.Clamp(pos.x, minScreenBounds.x, maxScreenBounds.x);
		pos.y = Mathf.Clamp(pos.y, minScreenBounds.y + heroBounds.extents.y, maxScreenBounds.y - (heroBounds.extents.y * 2f));
		hero.position = pos;
	}

	/// <summary>
	/// Calibrates the accelerometer.
	/// </summary>
	private void CalibrateAccelerometer()
	{
		Vector3 currentAcceleration = Input.acceleration;

		Quaternion rotateQuaternion = Quaternion.FromToRotation(new Vector3(0f, 0f, -1f), currentAcceleration);
		//create identity matrix ... rotate our matrix to match up with down vec
		Matrix4x4 matrix = Matrix4x4.TRS(Vector3.zero, rotateQuaternion, new Vector3(1f, 1f, 1f));
		//get the inverse of the matrix
		calibrationMatrix = matrix.inverse;
	}

	/// <summary>
	/// Gets the calibrated accelerometer input
	/// </summary>
	Vector3 getAccelerometer(Vector3 accelerator)
	{
		Vector3 accel2 = this.calibrationMatrix.MultiplyVector(accelerator);
		return accel2;
	}

	/// <summary>
	/// Loads the Voice Over Intro.
	/// </summary>
	void LoadVOIntro()
	{
		string[] startSounds;
		if(PlayerPrefs.GetInt ("WGCurrentLevel") == 1)
			startSounds	= new string[]{ "TipUp", "Collect10", "TouchWordGirl" };
		else
			startSounds	= new string[]{ "Collect10", "TouchWordGirl" };

		SoundManager.LoadVoiceOver ("Generic");
		SoundManager.LoadVoiceOver ("Villian");

		SoundManager.PlayVoiceOverChain (startSounds, "Generic", 0, false);
	}

	/// <summary>
	/// Loads the back ground music.
	/// </summary>
	void LoadBackGroundMusic()
	{
		if (SoundManager.Instance != null)
		{
			switch (StaticData.bgImagePrefix)
			{
			case "cityBackground_": 
				SoundManager.Instance.PlayMusic ("wgflying loop03");
				break;
			case "Night_BG_0": 
				SoundManager.Instance.PlayMusic ("wgflying loop02");
				break;
			case "School_BG_0": 
				SoundManager.Instance.PlayMusic ("wgflying loop03");
				break;
			case "Haze_BG_0": 
				SoundManager.Instance.PlayMusic ("wgflying loop01");
				break;
			default: 
				SoundManager.Instance.PlayMusic ("wgflying loop01");
				break;
			}
		}
	}

	/// <summary>
	/// Loads the backgrounds and place.
	/// </summary>
	void LoadBGAndPlace()
	{
		LoadBG(StaticData.bgImagePrefix.TrimEnd('0') + "Full");
	}

	/// <summary>
	/// Loads the Background
	/// </summary>
	void LoadBG(string bgName)
	{
		GameObject bg1 = GameObject.Instantiate(Resources.Load("Images/Prefabs/BG_Prefab_Sprite")) as GameObject;
		bg1.name = bgName;
		SpriteRenderer bgSprite = bg1.GetComponent<SpriteRenderer> ();

		string pathName = "Images/Backgrounds/" + bgName + ".png";
		TextAsset ta = Resources.Load <TextAsset>(pathName);

		Texture2D texture2d = new Texture2D (1,1);
		texture2d.LoadImage (ta.bytes);
		texture2d.wrapMode = TextureWrapMode.Repeat;


		float bgScale = 3.38f;
		bg1.transform.localScale = new Vector3 (bgScale, bgScale, bgScale);

		bgSprite.sprite = Sprite.Create (texture2d, new Rect(0,0,texture2d.width,texture2d.height),new Vector2(.5f,.5f),1f);

		bgTrans [0] = bg1.transform;

		bg1.transform.position = new Vector3(txScaleWidth, bg1.transform.position.y, 50f);
	}
		
	public void PauseScrollBG()
	{
		pauseScrollBG = true;
		waitTime = Time.time + pauseScrollTime;

		bgScrollSpeed = 0;
		scrollSpeed = 0;
	}


	void ClearAndLoadObjects()
	{
		ClearLevelObjects ();
		LoadLevelObjects ();
	}

	void ClearLevelObjects()
	{
		for (int i = 0; i < aiObjs.Count; i++)
		{
			GameObject.DestroyImmediate (aiObjs[i]);
		}
		aiObjs.Clear ();
	}
		
	void LoadLevelObjects()
	{
		for (int i = 0; i < StaticData.aiTypes.Count; i++)
		{
			LoadObject (StaticData.aiTypes[i]);
		}
	}

	void LoadObject(StaticData.AIType aiType)
	{
		GameObject aiObj = GameObject.Instantiate(Resources.Load("Objects/Prefabs/" + aiType.aiType )) as GameObject;
		aiObj.transform.position = aiType.pos;
		aiObj.name = aiType.aiType;
		aiObj.transform.parent = scrollBgRoot.transform;
		aiObjs.Add(aiObj);

		float vPosAdj = 0;
		float hPosAdj = 0;

		Bounds boundsObj = new Bounds();
		if (aiObj.GetComponent<PolygonCollider2D> () != null)
		{
			boundsObj = aiObj.GetComponent<PolygonCollider2D> ().bounds;
			vPosAdj = 768 - boundsObj.extents.y;
		}


		switch (aiType.aiType)
		{

		case "bg_face":
			vPosAdj -= 142f;
				StartCoroutine(BirthdayGirlSound(aiObj.transform));
			break;
		case "robot_face":
			vPosAdj = 97f;
				StartCoroutine(RobotFaceSound(aiObj.transform));
			break;
		case "GoldRobot_face":
			vPosAdj += 63f;
				StartCoroutine(GoldRobotSound(aiObj.transform));
			break;
		case "ENERGY_MONSTER_face":
				StartCoroutine(EnergyMonsterSound(aiObj.transform));
			break;
		case "helicopter":
				StartCoroutine( HelicopterAnimate(aiObj.transform) );
			break;
		case "Blimp":
			Vector3 moveToPos = new Vector3( (aiType.pos.x * 4.267f - 1024) + boundsObj.extents.x, (( txScaleHeight - aiType.pos.y * 4.8f) - txScaleHeight + vPosAdj), 0f);
				BlimpAnimate(aiObj.transform, moveToPos);
			break;
		case "Granny":
			StartCoroutine (GrannyAnimate (aiObj.transform));
			break;
		case "Tobey_Face":	
			vPosAdj -= 32f;
				StartCoroutine(TobeyFaceSound(aiObj.transform));
			break;
		case "Word":
			aiObj.GetComponent<tk2dTextMesh> ().text = StaticData.correctWord.ToUpper ();
			aiObj.GetComponent<tk2dTextMesh> ().ForceBuild ();

			BoxCollider2D aiBoxCol = aiObj.GetComponent<BoxCollider2D> ();
			aiBoxCol.size = aiObj.GetComponent<MeshRenderer> ().bounds.size;
			aiBoxCol.offset = new Vector2(0f, (aiBoxCol.size.y/2f * -1f));
			vPosAdj = 768 - aiBoxCol.bounds.extents.y;
			break;
		default:
			Debug.LogError ("AI Type Not Found: " + aiType.aiType);
			break;
		}

		if(aiObj.GetComponent<PolygonCollider2D> () != null)
			hPosAdj = aiObj.GetComponent<PolygonCollider2D> ().bounds.extents.x;
		else
			hPosAdj = aiObj.GetComponent<MeshRenderer> ().bounds.extents.x;

		aiObj.transform.localPosition = new Vector2( (aiType.pos.x * 4.267f - 1024) + hPosAdj, (( txScaleHeight - aiType.pos.y * 4.8f) - txScaleHeight + vPosAdj));
	}
		
	/// <summary>
	/// Waits for player to arrive close to granny and launchers her forward
	/// </summary>
	/// <returns>The animate.</returns>
	IEnumerator GrannyAnimate(Transform objTrans)
	{
		while (missionPlaying == false)
			yield return null;
		
		float grannyLaunchDis = 1800f;
		while(panelCount > 10)
		{
			yield return null;
		}

		while (hero.position.x < (objTrans.position.x - grannyLaunchDis))
		{
			yield return null;
		}

		int ranNum = UnityEngine.Random.Range (1, 6);
		if (ranNum == 1)
			SoundManager.PlayVoiceOver ("GrannyMay2", "Villian");
		else
			SoundManager.PlayVoiceOver ("GrannyFlying_5sec", "Villian");
		
		objTrans.DOLocalMoveX (-4000f, 6f).SetRelative (true);

	}

	/// <summary>
	/// Animates the blimp in a figure 8
	/// </summary>
	void BlimpAnimate(Transform objTrans, Vector3 startPos)
	{
		float offset = 40f;
		Vector3 point1 = startPos + new Vector3 (-offset, offset, 0);
		Vector3 point2 = startPos + new Vector3 (offset, -offset, 0);
		Vector3 point3 = startPos + new Vector3 (offset, offset, 0);
		Vector3 point4 = startPos + new Vector3 (-offset, -offset, 0);
		Vector3 point5 = startPos;
		Vector3 point6 = startPos + new Vector3 (-offset * 2f - 10f, 0f, 0f);
		Vector3 point7 = startPos + new Vector3 (offset * 2f + 10f, 0f, 0f);
		Vector3 point8 = startPos + new Vector3 (offset * 1.75f + 10f, offset/2f + 10f, 0f);
		Vector3 point9 = startPos + new Vector3 (offset * 1.75f + 10f, -offset/2f - 10f, 0f);
		Vector3 point10 = startPos + new Vector3 (-offset * 1.75f - 10f, offset/2f + 10f, 0f);
		Vector3 point11 = startPos + new Vector3 (-offset * 1.75f - 10f, -offset/2f - 10f, 0f);

		Vector3[] pathP = new Vector3[]{ point1, point10, point6, point11, point4, point3, point8, point7, point9, point2, point5 };

		objTrans.DOLocalPath (pathP, 4f, PathType.CatmullRom, PathMode.Sidescroller2D).SetLoops (-1, LoopType.Restart).SetEase (Ease.Linear);
	}

	/// <summary>
	/// Animates the Helicopter up and down
	/// </summary>
	IEnumerator HelicopterAnimate(Transform objTrans)
	{
		objTrans.DOLocalMoveY (-80f, .8f).SetLoops (-1, LoopType.Yoyo).SetEase(Ease.InOutQuad).SetRelative(true);

		while (missionPlaying == false)
			yield return null;

		float initiateDistance = 1800f;
		while(panelCount > 10)
		{
			yield return null;
		}

		while (hero.position.x < (objTrans.position.x - initiateDistance))
		{
			yield return null;
		}

		int ranNum = UnityEngine.Random.Range (1, 6);
		if (ranNum == 1)
			SoundManager.PlayVoiceOver ("Helicopter1", "Villian");
		else
			SoundManager.PlayVoiceOver ("Helicopter_5sec", "Villian");
	}

	/// <summary>
	/// Loads in the Sound of the Energy Monster up and down
	/// </summary>
	IEnumerator EnergyMonsterSound(Transform objTrans)
	{
		while (missionPlaying == false)
			yield return null;

		float initiateDistance = 1800f;
		while(panelCount > 10)
		{
			yield return null;
		}

		while (hero.position.x < (objTrans.position.x - initiateDistance))
		{
			yield return null;
		}

		int ranNum = UnityEngine.Random.Range (1, 7);
		if (ranNum == 1)
			SoundManager.PlayVoiceOver ("TheEnergyMonster2", "Villian");
		else
			SoundManager.PlayVoiceOver ("EnergyMonster_5sec", "Villian");
	}

	/// <summary>
	/// Loads in the Sound of the Gold Robot up and down
	/// </summary>
	IEnumerator GoldRobotSound(Transform objTrans)
	{
		while (missionPlaying == false)
			yield return null;

		float initiateDistance = 1800f;
		while(panelCount > 10)
		{
			yield return null;
		}

		while (hero.position.x < (objTrans.position.x - initiateDistance))
		{
			yield return null;
		}

		int ranNum = UnityEngine.Random.Range (1, 6);
		if (ranNum == 8)
			SoundManager.PlayVoiceOver ("GoldenRobot1", "Villian");
	}

	/// <summary>
	/// Loads in the Sound of the Robot Face up and down
	/// </summary>
	IEnumerator RobotFaceSound(Transform objTrans)
	{
		while (missionPlaying == false)
			yield return null;

		float initiateDistance = 1800f;
		while(panelCount > 10)
		{
			yield return null;
		}

		while (hero.position.x < (objTrans.position.x - initiateDistance))
		{
			yield return null;
		}

		int ranNum = UnityEngine.Random.Range (1, 10);
		if (ranNum == 8)
			SoundManager.PlayVoiceOver ("Robot2", "Villian");
	}

	/// <summary>
	/// Loads in the Sound of the Tobey Face up and down
	/// </summary>
	IEnumerator TobeyFaceSound(Transform objTrans)
	{
		while (missionPlaying == false)
			yield return null;

		float initiateDistance = 1800f;
		while(panelCount > 10)
		{
			yield return null;
		}

		while (hero.position.x < (objTrans.position.x - initiateDistance))
		{
			yield return null;
		}

		int ranNum = UnityEngine.Random.Range (1, 5);
		if (ranNum == 8)
			SoundManager.PlayVoiceOver ("Tobey2", "Villian");
	}

	/// <summary>
	/// Loads in the Sound of the Birthday Girl up and down
	/// </summary>
	IEnumerator BirthdayGirlSound(Transform objTrans)
	{
		while (missionPlaying == false)
			yield return null;

		float initiateDistance = 1800f;
		while(panelCount > 10)
		{
			yield return null;
		}

		while (hero.position.x < (objTrans.position.x - initiateDistance))
		{
			yield return null;
		}

		int ranNum = UnityEngine.Random.Range (1, 6);
		if (ranNum == 8)
			SoundManager.PlayVoiceOver ("BirthdayGirl2", "Villian");
	}


	/// <summary>
	/// Adds a point to the players score
	/// </summary>
	public void PlayerAddPoint()
	{
		playerPoints++;

		hudScript.UpdateStars (playerPoints);

		if (playerPoints >= HUD.maxPoints)
		{
			PlayerWins ();
		}
	}

	/// <summary>
	/// Called when the player wins the mission and sets to load comic scene.
	/// </summary>
	void PlayerWins()
	{
		missionPlaying = false;

		bool isMissionStarting = false;

		PlayerPrefs.SetString ("WGisMissionStarting", isMissionStarting.ToString ());

		hero.GetComponent<Player>().PlayLoopDaLoop();

		StartCoroutine (WaitForHeroFlyOff ());
	}

	/// <summary>
	/// Waits for hero to fly off screen.
	/// </summary>
	IEnumerator WaitForHeroFlyOff()
	{
		while (hero.GetComponent<Player> ().isHeroFlyingOff)
		{
			yield return null;
		}

		UnityEngine.SceneManagement.SceneManager.LoadScene ("Comic");
	}

	/// <summary>
	/// Loads back to the level selection menu
	/// </summary>
	void ButtonBackToLvlSelection()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene("Title");
	}
}

