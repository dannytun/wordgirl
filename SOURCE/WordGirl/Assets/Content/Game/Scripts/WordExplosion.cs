﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Attached to Word Objects to handle Animating Word Explosion and its removal
/// </summary>
public class WordExplosion : MonoBehaviour 
{
	private tk2dSpriteAnimator explosionAnimator;

	void Start()
	{
		explosionAnimator = this.GetComponent<tk2dSpriteAnimator> ();
		explosionAnimator.Play ();
	}

	void Update()
	{
		if (explosionAnimator.IsPlaying("Explosion") == false)
		{
			GameObject.Destroy (this.gameObject);
		}
	}
}
