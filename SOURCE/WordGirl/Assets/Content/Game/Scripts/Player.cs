﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Player : MonoBehaviour 
{
	private GamePlay gamePlayScript;

	private tk2dSprite playerSprite;

	private float waitTime = 5f;
	private float blinkTime = .8f;
	private float playerYoYoSpeed = 50f;
	private float playerKnockBackDistance = 170f;
	private float screenWidth;
	private float playerWidth = 300f;

	public bool isHeroFlyingOff = false;
	public bool isHeroWaiting = false;
	private bool isFirstWordCollected = false;

	private string[] hitSounds = new string[] {"Oooph", "Ahh2", "Uuuph", "Wooh"};

	void Start()
	{
		gamePlayScript = GameObject.Find ("GamePlay").GetComponent<GamePlay>();
		playerSprite = this.gameObject.GetComponent<tk2dSprite> ();

		screenWidth = GameObject.Find ("tk2dCamera").GetComponentInChildren<tk2dCameraAnchor> ().transform.position.x;
		playerWidth = this.GetComponent<MeshRenderer> ().bounds.size.x;

		isFirstWordCollected = false;
		isHeroWaiting = true;
		StartCoroutine (WaitForPlayerToStart ());
		StartCoroutine (Blink(.3f, 5));

		playerKnockBackDistance = Mathf.Abs(screenWidth + this.transform.position.x) - (playerWidth/2);
	}

	/// <summary>
	/// Detects if the player collides with an Enemy or Word obj
	/// </summary>
	void OnTriggerEnter2D(Collider2D col)
	{
		if (isHeroFlyingOff)
			return;
		
		if (col.gameObject.tag == "Enemy")
		{
			PlayerHitEnemy ();
		}
		else if (col.gameObject.tag == "Word")
		{
			PlayerHitWord ();
			col.gameObject.SendMessage ("CollectWord");
		}
	}

	/// <summary>
	/// Called if the player hits an enemy to knock back the player
	/// </summary>
	void PlayerHitEnemy()
	{
		StartCoroutine (BlinkPlayer ());

		StartCoroutine (KnockBackPlayer ());

		gamePlayScript.PauseScrollBG ();

		PlayRandomHitSound();
	}

	/// <summary>
	/// Random sound played if hitting an enemy
	/// </summary>
	void PlayRandomHitSound()
	{
		SoundManager.PlaySound (hitSounds [Random.Range (0, 3)]);
	}

	/// <summary>
	/// Called if the player hits a word
	/// </summary>
	void PlayerHitWord()
	{
		gamePlayScript.PlayerAddPoint();
		if(isFirstWordCollected)
			SoundManager.PlaySound ("Collect word");
		else
		{
			isFirstWordCollected = true;

			string audioLocation = "Sounds/VoiceOver/Words/" + StaticData.correctWord + "_WG";

			this.gameObject.AddComponent<AudioSource> ();
			AudioSource vocalSource =  this.gameObject.GetComponent<AudioSource> ();
			vocalSource.clip = AudioClip.Instantiate(Resources.Load(audioLocation)) as AudioClip;
			vocalSource.Play ();

			SoundManager.PlaySound ("Collect word");
		}
	}
		
	/// <summary>
	/// Dissables the player collision.
	/// </summary>
	void EnablePlayerCollision(bool isEnabled)
	{
		this.gameObject.GetComponent<BoxCollider2D> ().enabled = isEnabled;
	}


	/// <summary>
	/// Waits for player to start.
	/// </summary>
	IEnumerator WaitForPlayerToStart()
	{
		while (isHeroWaiting)
		{
			float ranNum = Random.Range (50, 150);
			yield return StartCoroutine (PlayerYoYo (ranNum));
		}
	}

	/// <summary>
	/// Moves the player up and down while waiting at start
	/// </summary>
	IEnumerator PlayerYoYo(float randomNumber)
	{
		Transform playerTrans = this.gameObject.transform;
		Vector3 playerStartingPos = playerTrans.position;
		Vector3 moveToPos = playerStartingPos + (Vector3.down * randomNumber);

		while (playerTrans.position.y -1f > moveToPos.y)
		{
			playerTrans.position = Vector3.MoveTowards (playerTrans.position, moveToPos, playerYoYoSpeed * Time.deltaTime);
			yield return null;
		}
			
		while (playerTrans.position.y < playerStartingPos.y - 1f)
		{
			playerTrans.position = Vector3.MoveTowards (playerTrans.position, playerStartingPos, playerYoYoSpeed * Time.deltaTime);
			yield return null;
		}
	}

	/// <summary>
	/// Fade the player in and out.
	/// </summary>
	IEnumerator Fade (float start, float end, float length, tk2dSprite sprite) 
	{ 
		if (sprite.color.a == start)
		{
			for (float i = 0f; i < 1f; i += Time.deltaTime*(1f/length)) 
			{
				sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, Mathf.Lerp(start, end, i));
				yield return null;

			} 
		} 
		sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, end);
	} 

	/// <summary>
	/// Sets the player to keep fadding in and out until the gameplay starts
	/// </summary>
	IEnumerator Blink (float blinkSpeed, int blinkCount)
	{
		int currentCount = 0;

		yield return new WaitForSeconds (5f);

		while (isHeroWaiting)
		{
			while (currentCount < blinkCount)
			{
				yield return StartCoroutine (Fade (1f, 0f, blinkSpeed, playerSprite));
				yield return StartCoroutine (Fade (0f, 1f, blinkSpeed, playerSprite));
				currentCount++;
			}
			playerSprite.color = Color.white;
			currentCount = 0;
			yield return new WaitForSeconds (5f);
		}
	}

	/// <summary>
	/// Turns off player colliders and causes the player to blink
	/// </summary>
	IEnumerator BlinkPlayer()
	{
		float timeToWait = Time.time + waitTime;

		EnablePlayerCollision(false);

		while(Time.time < timeToWait)
		{
			playerSprite.color = Color.Lerp(Color.white, Color.clear, Mathf.PingPong(Time.time, blinkTime));
			yield return null;
		}

		EnablePlayerCollision(true);

		playerSprite.color = Color.white;
	}

	/// <summary>
	/// Moves the player back and then restores the player to its starting pos
	/// </summary>
	IEnumerator KnockBackPlayer()
	{
//		Transform playerTrans = this.gameObject.transform;
//		Vector3 playerStartingPos = playerTrans.position;
//		Vector3 moveToPos = playerStartingPos - (Vector3.right * playerKnockBackDistance);

//		while (playerTrans.position.x -1f > moveToPos.x)
//		{
//			playerTrans.position = Vector3.MoveTowards (playerTrans.position, moveToPos, playerKnockBackSpeed * Time.deltaTime);
//
//			yield return null;
//		}

		Tween heroTween = this.transform.DOMoveX (-playerKnockBackDistance, 1f).SetEase (Ease.OutCirc).SetRelative (true);
		yield return heroTween.WaitForCompletion ();

		yield return new WaitForSeconds (1f);

		heroTween = this.transform.DOMoveX (playerKnockBackDistance, 2f).SetEase (Ease.InOutBack).SetRelative (true);
		yield return heroTween.WaitForCompletion ();
//
//		while (playerTrans.position.x < playerStartingPos.x - 1f)
//		{
//			playerTrans.position = Vector3.MoveTowards (playerTrans.position, playerStartingPos, playerKnockBackSpeed * Time.deltaTime * .5f);
//
//			yield return null;
//		}
	}

	/// <summary>
	/// Plaies the loop da loop animation and sound.
	/// </summary>
	public void PlayLoopDaLoop()
	{
		isHeroFlyingOff = true;
		EnablePlayerCollision (false);
		SoundManager.PlaySound ("loopDloop_sfx");
		this.GetComponent<tk2dSpriteAnimator> ().Play ("LoopDLoop");
		this.GetComponent<tk2dSprite> ().scale = new Vector3(4f, 4f, 4f);
		this.transform.position += Vector3.up * 138f;

		StartCoroutine (MovePlayerOffScreen ());
	}

	/// <summary>
	/// Moves the player off screen.
	/// </summary>
	IEnumerator MovePlayerOffScreen()
	{
		while (this.GetComponent<tk2dSpriteAnimator> ().IsPlaying("LoopDLoop"))
		{
			yield return null;

		}
		this.GetComponent<tk2dSpriteAnimator> ().Play ("Hero");
		this.GetComponent<tk2dSprite> ().scale = new Vector3(1f, 1f, 1f);
		this.transform.position -= Vector3.up * 138f;
		Tween heroTween = this.transform.DOMoveX (screenWidth + playerWidth, 0.25f).SetEase (Ease.InCirc).SetRelative (true);
		yield return heroTween.WaitForCompletion ();

		isHeroFlyingOff = false;
	}
}
