﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour 
{
	public const int maxPoints = 10;
	private tk2dSprite[] starArray = new tk2dSprite[10];

	void Start()
	{
		tk2dCamera tkCam = GameObject.Find ("tk2dCamera").GetComponent<tk2dCamera>();
		this.transform.position = new Vector3 (0f, tkCam.nativeResolutionHeight / 2, -1f);

		for(int i = 0; i < maxPoints; i++)
		{
			starArray[i] = this.transform.GetChild (i).GetComponent<tk2dSprite>();
		}
	}

	/// <summary>
	/// Updates the star shield points.
	/// </summary>
	public void UpdateStars(int playerPoints)
	{
		if(starArray[playerPoints -1] != null)
			starArray [playerPoints - 1].SetSprite ("red_star");
	}
}
