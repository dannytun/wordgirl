﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Attached to Word
/// </summary>
public class Word : MonoBehaviour 
{
	public void CollectWord()
	{
		GameObject explosion = GameObject.Instantiate( Resources.Load ("Objects/Prefabs/Explosion")) as GameObject;
		explosion.transform.parent = this.transform.parent;
		explosion.transform.position = this.transform.position;
		Destroy (this.gameObject);
	}

}
