﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using SimpleJSON;

public class Title : MonoBehaviour 
{
	List<tk2dUIItem> items = new List<tk2dUIItem>();
	public bool isLoading = false;
	private bool isMoreAppsActive = false;
	private string privacyHtmlFile = "privacy.html";
	private GameObject moreBtn;
	private int requestTryCount = 3;
	private tk2dCamera cam;
	private string url = "http://livestage.curiousmedia.com/public/sch/more-apps/more_setting.json";

	void OnEnable()
	{
		foreach (tk2dUIItem item in items)
		{
			if (item != null)
			{
				item.OnClickUIItem += HandleClick;
				item.OnDownUIItem += OnDown;
				item.OnReleaseUIItem += OnRelease;
			}
		}
	}

	void OnDisable()
	{
		foreach (tk2dUIItem item in items)
		{
			if (item != null)
			{
				item.OnClickUIItem -= HandleClick;
				item.OnDownUIItem -= OnDown;
				item.OnReleaseUIItem -= OnRelease;
			}
		}
	}

	// Use this for initialization
	void Start () 
	{
		Init ();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
	}

	void Init()
	{
		Screen.sleepTimeout = SleepTimeout.SystemSetting;

		StartCoroutine(InitCheckMoreApps());

		LoadBackground ();
		LoadButtons ();

		StaticData.ClearData ();
		StaticData.LoadJsonLevelData (PlayerPrefs.GetInt("WGCurrentLevel"));

		if(SoundManager.Instance != null)
			SoundManager.Instance.PlayMusic ("mainmenu_loop");
	}

	/// <summary>
	/// Loads the background.
	/// </summary>
	void LoadBackground()
	{
		GameObject bg = GameObject.Instantiate(Resources.Load("Images/Prefabs/BG_Prefab")) as GameObject;
		bg.name = "TitleBackground";
		bg.transform.position = new Vector3(0f, 0f, 1f);
		tk2dSpriteFromTexture bgSprite = bg.GetComponent<tk2dSpriteFromTexture> ();

		string pathName = "Images/Menus/Title.png";
		TextAsset ta = Resources.Load <TextAsset>(pathName);

		Texture2D texture2d = new Texture2D (1,1);
		texture2d.LoadImage (ta.bytes);
		texture2d.wrapMode = TextureWrapMode.Clamp;

		bgSprite.Create (tk2dSpriteCollectionSize.Default(), texture2d, tk2dBaseSprite.Anchor.MiddleCenter);
		bgSprite.spriteCollectionSize.pixelsPerMeter = 1f;
		bgSprite.ForceBuild ();
		bg.GetComponent<AutoScaleTexture> ().ScaleTexture (2852f, 1536f);
	}

	/// <summary>
	/// Loads the buttons.
	/// </summary>
	void LoadButtons()
	{
		cam = GameObject.Find("tk2dCamera").GetComponent<tk2dCamera>();

		//Play Button
		GameObject btn = GameObject.Instantiate(Resources.Load("Objects/Prefabs/GenericButton")) as GameObject;
		btn.name = "Play";
		btn.transform.position = new Vector3(-693f, -360f, 0f);
		btn.GetComponent<tk2dSprite> ().SetSprite ("PlayBtn");
		btn.GetComponentInChildren<tk2dTextMesh> ().text = "PLAY";
		btn.GetComponentInChildren<tk2dTextMesh> ().scale = new Vector3 (1.4f, 1.4f, 1.4f);

		BoxCollider2D btnCol = btn.GetComponent<BoxCollider2D>();
		btnCol.size = btn.GetComponent<MeshRenderer>().bounds.size;
		SetButtonHandles(btn);

		//Privacy Button
		btn = GameObject.Instantiate(btn) as GameObject;
		btn.name = "Privacy";
		btn.transform.position = new Vector3(-759f, -617f, 0f);
		btn.GetComponentInChildren<tk2dTextMesh> ().text = "Privacy";
		btn.GetComponentInChildren<tk2dTextMesh> ().scale = new Vector3 (.9f, .9f, .9f);
		btn.GetComponent<tk2dSprite> ().scale = new Vector3 (.75f, .73f, .7f);

		btnCol = btn.GetComponent<BoxCollider2D>();
		btnCol.size = btn.GetComponent<MeshRenderer>().bounds.size;

		SetButtonHandles(btn);
	}

	/// <summary>
	/// Sets the button handles.
	/// </summary>
	void SetButtonHandles(GameObject btn)
	{
		items.Add (btn.GetComponent<tk2dUIItem> ());

		btn.GetComponent<tk2dUIItem> ().OnClickUIItem += HandleClick;
		btn.GetComponent<tk2dUIItem> ().OnDownUIItem += OnDown;
		btn.GetComponent<tk2dUIItem> ().OnReleaseUIItem += OnRelease;
	}

	/// <summary>
	/// Handles the button press.
	/// </summary>
	void HandleClick(tk2dUIItem clickedItem)
	{
		if (isLoading == false)
		{
			switch (clickedItem.name)
			{
			case "Play":
				StartCoroutine (PlayGameBtn ());
				break;
			case "Privacy":
				PrivacyBtn ();
				break;
			case "More":
				MoreBtn ();
				break;
			}
		}
	}

	/// <summary>
	/// On press release scales up the button
	/// </summary>
	void OnRelease(tk2dUIItem clickedItem)
	{
		DOTween.Kill(clickedItem.transform);
		clickedItem.transform.DOScale(Vector3.one * 1f, 0.25f).SetEase(Ease.OutBack, 5f);
	}

	/// <summary>
	/// On press down scales down the button
	/// </summary>
	void OnDown(tk2dUIItem clickedItem)
	{
		DOTween.Kill(clickedItem.transform);
		clickedItem.transform.localScale = Vector3.one * 0.8f;
	}


	IEnumerator PlayGameBtn()
	{
		isLoading = true;
		SoundManager.PlaySound ("Play");
		while(SoundManager.isPlayingSound("Play"))
		{
			yield return null;
		}

		UnityEngine.SceneManagement.SceneManager.LoadScene ("Comic");
	}
		
	/// <summary>
	/// Privacy Button
	/// </summary>
	void PrivacyBtn()
	{
		//Add Privacy Menu Call
		LoadFromFile ();
	}

	/// <summary>
	/// More Button
	/// </summary>
	void MoreBtn()
	{
		if(isLoading == false)
		{
			isLoading = true;
			GameObject parentGate = GameObject.Instantiate(Resources.Load("Objects/Prefabs/ParentGate")) as GameObject;
		}
	}

	/// <summary>
	/// Loads from file.
	/// </summary>
	void LoadFromFile() 
	{

		UniWebView webView = CreateWebView();
		webView.url = UniWebViewHelper.streamingAssetURLForPath(privacyHtmlFile);
		webView.Load();
		webView.Show();
	}

	/// <summary>
	/// Creates the Uni Web View Object.
	/// </summary>
	UniWebView CreateWebView() 
	{
		GameObject webViewGameObject = GameObject.Find("WebView");
		if (webViewGameObject == null) 
		{
			webViewGameObject = new GameObject("WebView");
		}

		UniWebView webView = webViewGameObject.AddComponent<UniWebView>();

		webView.toolBarShow = true;
		return webView;
	}

	/// <summary>
	/// Creates the More Apps Button.
	/// </summary>
	void CreateMoreButton()
	{
		moreBtn = ObjectLoader.LoadButton("TitleCollection", "More", "Btn_More_Normal", new Vector3(cam.ScreenExtents.x + 120f, 633f, -1f)); //537 396
		SetButtonHandles (moreBtn);
	}

	/// <summary>
	/// Inits the check more apps to enable the button.
	/// </summary>
	IEnumerator InitCheckMoreApps()
	{
		yield return StartCoroutine(LoadExternalJsonFile());

		if(isMoreAppsActive && moreBtn == null)
		{
			CreateMoreButton();
		}
	}

	/// <summary>
	/// Load json settings file
	/// </summary>
	IEnumerator LoadExternalJsonFile()
	{
		WWW www = new WWW(url);
		yield return www;

		int tryCount = 0;

		while(tryCount < requestTryCount)
		{
			if(www.error != null)
			{
				Debug.LogError("web access error: " + www.error);
				tryCount++;
			}
			else
			{
				JSONNode node = JSON.Parse(www.text);
				isMoreAppsActive = node["isMoreAppsActive"].AsBool;
				yield break;
			}
		}
	}
}
