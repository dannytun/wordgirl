﻿using UnityEngine;
using System.Collections;

public class Init : MonoBehaviour 
{
	public GameObject[] logoTextures;
	private bool is16x9 = false;

	void Start () 
	{
		Application.targetFrameRate = 60;
		AdjustQuality();
		StartCoroutine(Initialize());
	}

	IEnumerator Initialize()
	{
		yield return StartCoroutine(LoadSplashScreens());

		CheckGameData ();

		LoadTitleMenu ();
	}

	/// <summary>
	/// Checks the game data at start and set default mission values if none exist
	/// </summary>
	void CheckGameData()
	{
		if( PlayerPrefs.GetInt ("WGCurrentLevel") == null || PlayerPrefs.GetInt ("WGCurrentLevel") ==  0)
			PlayerPrefs.SetInt ("WGCurrentLevel", 1);

		if(PlayerPrefs.GetString ("WGisMissionStarting") == null || PlayerPrefs.GetString("WGisMissionStarting") == "")
			PlayerPrefs.SetString ("WGisMissionStarting", "true");
	}

	void LoadTitleMenu()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Title");
	}

	IEnumerator LoadSplashScreens()
	{
		string videoSplashFile = "SCH_Splash_KBv5_3.2.mp4";
		if(is16x9)
		{
			videoSplashFile = "SCH_Splash_KBv5_16.9.mp4";
		}

		Handheld.PlayFullScreenMovie(videoSplashFile, Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFill);

		for(int i = 0; i < logoTextures.Length; i++)
		{
			logoTextures[i].SetActive(true);
			yield return new WaitForSeconds(2f);
			logoTextures[i].SetActive(false);
		}
	}

	private void AdjustQuality()
	{
		tk2dCamera cam = FindObjectOfType<tk2dCamera>();
		if (cam == null)
		{
			Debug.LogWarning("No tk2dCamera found. Not adjusting quality.");
		}

		Debug.Log("cam.TargetResolution: " + cam.TargetResolution);

		//Platform
		int displayWidth = Mathf.FloorToInt(cam.TargetResolution.x);
		int displayHeight = Mathf.FloorToInt(cam.TargetResolution.y);

		print("Display Size: " + displayWidth + ", " + displayHeight);

		//Swap
		if (displayWidth < displayHeight)
		{
			displayWidth = Mathf.FloorToInt(cam.TargetResolution.y);
			displayHeight = Mathf.FloorToInt(cam.TargetResolution.x);
		}

		print("Swapped Display Size: " + displayWidth + ", " + displayHeight);

		//Check
		if (displayWidth > 1024 || displayHeight > 768) //exclude non-retina ipad, but include iphone 5 and up
		{
			QualitySettings.SetQualityLevel(1); //Full Rez
		}
		else
		{
			QualitySettings.SetQualityLevel(0); //Half Rez
		}

		if(QualitySettings.GetQualityLevel() > 0 && (cam.TargetResolution.x / cam.TargetResolution.y) > 1.7f)
		{
			Debug.Log("16x9 set");
			is16x9 = true;
		}

		//Force Quality
		#if UNITY_IPHONE
		switch (UnityEngine.iOS.Device.generation)
		{
			case UnityEngine.iOS.DeviceGeneration.iPodTouch4Gen:
			case UnityEngine.iOS.DeviceGeneration.iPhone4:
			case UnityEngine.iOS.DeviceGeneration.iPhone4S:
			case UnityEngine.iOS.DeviceGeneration.iPad3Gen:
				QualitySettings.SetQualityLevel(0); //Half Rez
				break;
		}
		#endif

		Debug.LogWarning("Quality: " + QualitySettings.GetQualityLevel());
	}
}
