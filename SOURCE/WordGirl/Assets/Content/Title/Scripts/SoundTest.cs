﻿using UnityEngine;
using System.Collections;

public class SoundTest : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
		SoundManager.LoadVoiceOver ("Comic1");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			//SoundManager.PlaySound ("wa-button-press");
			SoundManager.PlayVoiceOver ("Comic1_A", "Comic1");
		}
	}
}
