﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class Comic : MonoBehaviour 
{
	private int currentLevel = 1;
	private int comicStripPanelCount = 1;
	private int maxPanel = 4;
	private tk2dSpriteFromTexture[] comicSprite = new tk2dSpriteFromTexture[4];
	public AudioSource vocalSource;
	private AudioClip currentAudioClip;
	private tk2dUIItem[] items = new tk2dUIItem[3];

	private bool isLoadingAudio = false;
	private bool isMissionStarting = true;
	private bool isLoading = false;


	void OnEnable()
	{
		foreach (tk2dUIItem item in items)
		{
			if (item != null)
			{
				item.OnClickUIItem += HandleClick;

				if (item.name != "ArrowBtn")
				{
					item.OnDownUIItem += OnDown;
					item.OnReleaseUIItem += OnRelease;
				}
			}
		}
	}

	void OnDisable()
	{
		foreach (tk2dUIItem item in items)
		{
			if (item != null)
			{
				item.OnClickUIItem -= HandleClick;

				if (item.name != "ArrowBtn")
				{
					item.OnDownUIItem -= OnDown;
					item.OnReleaseUIItem -= OnRelease;
				}
			}
		}
	}

	void Start()
	{
		Screen.sleepTimeout = SleepTimeout.SystemSetting;
		
		currentLevel = PlayerPrefs.GetInt ("WGCurrentLevel");
		isMissionStarting = bool.Parse( PlayerPrefs.GetString ("WGisMissionStarting"));
			
		if (currentLevel > 20)
			currentLevel -= 20;
		
		if(SoundManager.Instance != null)
			SoundManager.Instance.PlayMusic ("mainmenu_loop");

		isLoadingAudio = true;
		LoadComicStrip ();
	}


	void Update()
	{
		//	 Used for skipping comic panels for dev purposes only
//		if (Input.anyKeyDown)
//		{
//			StopAllCoroutines ();
//			if (isMissionStarting)
//				UnityEngine.SceneManagement.SceneManager.LoadScene ("Game");
//			else
//				LoadTitleMenu ();
//		}

		if (Input.GetKeyDown (KeyCode.Escape))
			Application.Quit ();
	}


	/// <summary>
	/// Initializes and starts the loading of the comic strip.
	/// </summary>
	void LoadComicStrip()
	{
		//Set Max Panels to scroll to before ending
		if (isMissionStarting)
		{
			comicStripPanelCount = 0;
			maxPanel = 4;
		}
		else
		{
			comicStripPanelCount = 4;
			maxPanel = 8;
		}

		LoadComicStripBackgrounds ();
		StartCoroutine (NextPanel ());
	}
		
	/// <summary>
	/// Loads the comic strip backgrounds.
	/// </summary>
	void LoadComicStripBackgrounds()
	{
		GameObject bg = GameObject.Instantiate(Resources.Load("Images/Prefabs/BG_Prefab")) as GameObject;
		bg.name = "ComicBackground";
		bg.transform.position = new Vector3(0f, 0f, 1f);
		tk2dSpriteFromTexture bgSprite = bg.GetComponent<tk2dSpriteFromTexture> ();

		string pathName = "Images/Comics/ComicBG" + ".png";
		TextAsset ta = Resources.Load <TextAsset>(pathName);

		Texture2D texture2d = new Texture2D (1,1);
		texture2d.LoadImage (ta.bytes);
		texture2d.wrapMode = TextureWrapMode.Clamp;

		bgSprite.Create (tk2dSpriteCollectionSize.Default(), texture2d, tk2dBaseSprite.Anchor.MiddleCenter);
		bgSprite.spriteCollectionSize.pixelsPerMeter = 1f;
		bgSprite.ForceBuild ();
		bg.GetComponent<AutoScaleTexture> ().ScaleTexture (2852f, 1536f);

		for (int i = 0; i < comicSprite.Length; i++)
		{
			GameObject comic = GameObject.Instantiate (Resources.Load ("Images/Prefabs/BG_Prefab")) as GameObject;
			comic.name = "ComicStrip";
			comic.transform.position = Vector3.zero;
			comicSprite[i] = comic.GetComponent<tk2dSpriteFromTexture> ();

			if (StaticData.comicStripSubObjs != null )
			{
				int subCount = 0;
				for (int index = 0; index < StaticData.comicStripSubObjs.Count; index++)
				{
					if (StaticData.comicStripSubObjs [index].panelIndexSubObjs == (comicStripPanelCount + i + 1))
					{
						LoadSubComicObj (comic, (comicStripPanelCount + i + 1), subCount, StaticData.comicStripSubObjs[index].subObjPos);
						subCount++;
					}
				}
			}
		}
	}

	/// <summary>
	/// Loads the sub comic object.
	/// </summary>
	void LoadSubComicObj(GameObject panelParentObj, int panelIndex, int index, Vector3 pos)
	{
		GameObject comicSubOjb = GameObject.Instantiate (Resources.Load ("Images/Prefabs/BG_Prefab")) as GameObject;
		comicSubOjb.name = "SubOjb";

		string pathName = "Images/Comics/level" + currentLevel + "ComicImage_" + panelIndex + "_sub" + index + ".png";
		TextAsset ta = Resources.Load <TextAsset>(pathName);

		Texture2D texture2d = new Texture2D (1,1);
		texture2d.LoadImage (ta.bytes);
		texture2d.wrapMode = TextureWrapMode.Clamp;

		comicSubOjb.GetComponent<tk2dSpriteFromTexture>().Create (tk2dSpriteCollectionSize.Default(), texture2d, tk2dBaseSprite.Anchor.MiddleCenter);
		comicSubOjb.transform.parent = panelParentObj.transform;
		comicSubOjb.transform.localPosition = pos;
		comicSubOjb.transform.localScale = Vector3.zero;

	}
		
	/// <summary>
	/// Loads the comic strip.
	/// </summary>
	void LoadComicStripPanel(int panelNumber)
	{
		string pathName = "Images/Comics/level" + currentLevel + "ComicImage_" + comicStripPanelCount + ".png";

		TextAsset ta = Resources.Load <TextAsset>(pathName);

		Texture2D texture2d = new Texture2D (1,1);
		texture2d.LoadImage (ta.bytes);
		texture2d.wrapMode = TextureWrapMode.Clamp;
		comicSprite[panelNumber - 1].Create (tk2dSpriteCollectionSize.Default(), texture2d, StaticData.comicStripPanels[comicStripPanelCount - 1].anchor);
		comicSprite [panelNumber - 1].transform.position = StaticData.comicStripPanels [comicStripPanelCount - 1].startingPos;
	}

	/// <summary>
	/// Animates the comic strip in.
	/// </summary>
	IEnumerator AnimateComicStripIn(int panelNumber)
	{
		yield return new WaitForSeconds (1f);

		float animTime = 0.3f;


		Tween comicTween;
		comicTween = comicSprite [panelNumber - 1].transform.DOMove (StaticData.comicStripPanels [comicStripPanelCount - 1].desPos, animTime, false).SetEase (Ease.OutCubic);

		SoundManager.PlaySound ("swipe");

		comicSprite [panelNumber - 1].transform.DOScaleX (0.5f, animTime / 2f).SetEase (Ease.InOutCubic).SetDelay (animTime / 4f);
		comicTween = comicSprite [panelNumber - 1].transform.DOScaleX (1.2f, 0.15f).SetEase (Ease.InOutCubic).SetDelay (animTime / 1.5f);

		yield return comicTween.WaitForCompletion ();
		comicSprite [panelNumber - 1].transform.DOScaleX (1f, 0.1f).SetEase (Ease.InOutCubic);

		comicTween = comicSprite [panelNumber - 1].transform.DOShakeScale (.2f, Vector3.left * .1f, 0, 0).SetDelay (.1f);
		yield return comicTween.WaitForCompletion ();

		yield return StartCoroutine(AnimateSubObj(comicSprite [panelNumber - 1].transform));
	}


	/// <summary>
	/// Pulses the comic strip.
	/// </summary>
	/// <returns>The comic strip.</returns>
	IEnumerator PulseComicStrip(int panelNumber)
	{
		yield return new WaitForSeconds (.8f);
		Tween comicTween;	

		string stringVal = ((char)(comicStripPanelCount + 64)).ToString();

		string audioName = "Comic" + currentLevel + "_" + stringVal;

		while (vocalSource.isPlaying && vocalSource.clip.name.Contains(audioName))
		{
			float animationDuration = 1.1f;
			comicTween = comicSprite [panelNumber - 1].transform.DOScale(.96f, animationDuration).SetEase(Ease.InOutQuad);
			yield return comicTween.WaitForCompletion();
			comicTween = comicSprite [panelNumber - 1].transform.DOScale (1f, animationDuration).SetEase(Ease.InOutQuad);
			yield return comicTween.WaitForCompletion();
		}
		comicSprite [panelNumber - 1].transform.DOScale (1f, .1f);
	}

	/// <summary>
	/// Animates the sub object.
	/// </summary>
	/// <returns>The sub object.</returns>
	IEnumerator AnimateSubObj(Transform parentObj)
	{
		List<Transform> subList = new List<Transform> ();

		for (int index = 0; index < parentObj.childCount; index++)
		{
			subList.Add(parentObj.GetChild(index));

			Tween subObjTween = subList [index].DOScale (Vector3.one, 0.4f).SetEase (Ease.OutBack, 5f);
			yield return subObjTween.WaitForCompletion ();

			yield return new WaitForSeconds (.3f);
		}

		for (int i = 0; i < subList.Count; i++)
		{
			subList [i].parent = null;
		}
	}

	IEnumerator AnimateComicStripOut(int panelNumber)
	{
		tk2dCamera tkCam = GameObject.Find ("tk2dCamera").GetComponent<tk2dCamera> ();
		SoundManager.PlaySound ("swipe");
		Tween comicTween = comicSprite[panelNumber].transform.DOMoveX (-tkCam.nativeResolutionWidth, .1f).SetEase (Ease.Linear).SetRelative (true);
		yield return comicTween.WaitForCompletion ();

		comicSprite[panelNumber].transform.position =new Vector3(tkCam.nativeResolutionWidth, 0f, 0f) ;
		comicTween = comicSprite[panelNumber].transform.DOMoveX (-tkCam.nativeResolutionWidth, .1f).SetEase (Ease.Linear).SetRelative (true);
		yield return comicTween.WaitForCompletion ();
	}

	/// <summary>
	/// Loads the Panel audio clip.
	/// </summary>
	void LoadPanelAudioClip()
	{
		//Get Character Value to load Audio A(65) to H(72)
		string stringVal = ((char)(comicStripPanelCount + 64)).ToString();

		string audioLocation = "Sounds/VoiceOver/Comic" + currentLevel + "/Comic" + currentLevel + "_" + stringVal;

		currentAudioClip = AudioClip.Instantiate(Resources.Load(audioLocation)) as AudioClip;
		vocalSource.clip = currentAudioClip;
		vocalSource.Play ();

		isLoadingAudio = false;
	}

	/// <summary>
	/// Loads the word audio clip.
	/// </summary>
	void LoadWordAudioClip(string AudioClipName)
	{
		string audioLocation = "Sounds/VoiceOver/" + AudioClipName;

		currentAudioClip = AudioClip.Instantiate(Resources.Load(audioLocation)) as AudioClip;
		vocalSource.clip = currentAudioClip;
		vocalSource.Play ();

		isLoadingAudio = false;
	}

	/// <summary>
	/// Cycles to the next comic strip
	/// </summary>
	IEnumerator NextPanel()
	{
		comicStripPanelCount++;

		int checkCount = 0;
		if (isMissionStarting == false)
			checkCount = 4;

		int panelIndex = comicStripPanelCount - checkCount;

		//Load Next Comic Image
		if (comicStripPanelCount < maxPanel)
		{
			isLoadingAudio = true;
			LoadComicStripPanel (panelIndex);
			yield return StartCoroutine (AnimateComicStripIn (panelIndex));

			LoadPanelAudioClip ();

			StartCoroutine (PulseComicStrip (comicStripPanelCount - checkCount));
			StartCoroutine (WaitToActivateNextPanel ());
		}
		else if (isMissionStarting && comicStripPanelCount == maxPanel)
		{
			isLoadingAudio = true;
			LoadComicStripPanel (panelIndex);
			yield return StartCoroutine (AnimateComicStripIn (panelIndex));

			LoadPanelAudioClip ();

			StartCoroutine (PulseComicStrip (comicStripPanelCount - checkCount));

			StartCoroutine (LoadWordSelectionPanel ());

		}
		else if (isMissionStarting == false && comicStripPanelCount == maxPanel)
		{
			//Load title screen
			isLoadingAudio = true;
			LoadComicStripPanel (panelIndex);
			yield return StartCoroutine (AnimateComicStripIn (panelIndex));

			LoadPanelAudioClip ();

			StartCoroutine (PulseComicStrip (comicStripPanelCount - checkCount));

			StartCoroutine (LoadNextButton ());
		}

		else
		{
			//Needs moved to new button function for next
			LoadTitleMenu ();
		}
		isLoading = false;
	}

	/// <summary>
	/// Waits till audio done to activate next panel.
	/// </summary>
	IEnumerator WaitToActivateNextPanel()
	{
		while (isLoadingAudio)
		{
			yield return null;
		}

		while (vocalSource.isPlaying)
		{
			yield return null;
		}

		StartCoroutine(NextPanel());
	}

	/// <summary>
	/// Checks the correct word button selected
	/// </summary>
	void WordBtn(tk2dUIItem clickedItem)
	{	
		StopCoroutine("WaitAnyWordAudio");

		clickedItem.transform.DOScale(Vector3.one * 1f, 0.25f).SetEase(Ease.OutBack, 5f);

		int correctWordIndex = 0;
		for (int i = 0; i < StaticData.wordsList.Count; i++)
		{
			if (StaticData.wordsList [i] == StaticData.correctWord)
				correctWordIndex = i;
		}

		if (clickedItem.name == correctWordIndex.ToString ())
		{
			StartCoroutine (CorrectWordChosen (int.Parse(clickedItem.name)));

			for (int i = 0; i < items.Length; i++)
			{
				if (items [i] != null && items [i].name != correctWordIndex.ToString ())
				{

					StartCoroutine (RemoveWord (int.Parse( items [i].name)));
				}
			}
		}
		else
		{
			StartCoroutine (WrongWordChosen (int.Parse(clickedItem.name)));
		}

		isLoading = false;
	}

	/// <summary>
	/// Removes the word button.
	/// </summary>
	IEnumerator RemoveWord (int btnID)
	{
		Tween btnTween =  items [btnID].transform.DOScale(0, 0.25f).SetEase(Ease.InBack, 3f);
		yield return btnTween.WaitForCompletion ();

		items [btnID].OnClickUIItem -= HandleClick;
		items [btnID].OnDownUIItem -= OnDown;
		items [btnID].OnReleaseUIItem -= OnRelease;
		Destroy (items [btnID].gameObject);
	}

	/// <summary>
	/// Correctly Chosen word loads the final VO and to game play.
	/// </summary>
	IEnumerator CorrectWordChosen(int btnID)
	{
		LoadWordAudioClip ("Words/" + StaticData.wordsList[btnID]);

		while (vocalSource.isPlaying)
		{
			yield return null;
		}

		LoadWordAudioClip ("Generic/ThatsRight");

		while (vocalSource.isPlaying)
		{
			yield return null;
		}

		LoadWordAudioClip ("Words/" + StaticData.wordsList[btnID] + "_definition");

		while (vocalSource.isPlaying)
		{
			yield return null;
		}

		yield return new WaitForSeconds (1f);

		UnityEngine.SceneManagement.SceneManager.LoadScene ("Game");
	}

	/// <summary>
	/// Wrongly chosen word loads incorrect VO and calls to remove the word button.
	/// </summary>
	IEnumerator WrongWordChosen(int btnID)
	{
		LoadWordAudioClip ("Words/" + StaticData.wordsList[btnID]);

		while (vocalSource.isPlaying)
		{
			yield return null;
		}

		LoadWordAudioClip ("Generic/ThatsNotIt");

		while (vocalSource.isPlaying)
		{
			yield return null;
		}

		LoadWordAudioClip ("Words/" + StaticData.wordsList[btnID] + "_definition");

		while (vocalSource.isPlaying)
		{
			yield return null;
		}

		StartCoroutine (RemoveWord (btnID));
	}

	/// <summary>
	/// Loads back to the title menu.
	/// </summary>
	void LoadTitleMenu()
	{
		isMissionStarting = true;

		int level = PlayerPrefs.GetInt ("WGCurrentLevel");
		level++;

		if (level > 40)
		{
			level = 1;
		}

		PlayerPrefs.SetInt ("WGCurrentLevel", level);
		PlayerPrefs.SetString ("WGisMissionStarting", isMissionStarting.ToString ());

		UnityEngine.SceneManagement.SceneManager.LoadScene ("Title");
	}
		
	IEnumerator LoadNextButton()
	{
		while (vocalSource.isPlaying)
		{
			yield return null;
		}

		GameObject btn = GameObject.Instantiate(Resources.Load("Objects/Prefabs/GenericButton")) as GameObject;
		btn.name = "Next";
		btn.transform.position = new Vector3(0f, -640f, 1f);
		btn.transform.localScale = Vector3.zero;

		btn.GetComponentInChildren<tk2dTextMesh> ().text = "NEXT";

		Tween btnTween =  btn.transform.DOScale(Vector3.one, 0.25f).SetEase(Ease.OutBack, 3f);
		yield return btnTween.WaitForCompletion ();


		BoxCollider2D btnCol = btn.GetComponent<BoxCollider2D>();
		btnCol.size = btn.GetComponent<MeshRenderer>().bounds.size;

		tk2dUIItem tkItem = btn.GetComponent<tk2dUIItem> ();
		items [0] = tkItem;
		tkItem.OnClickUIItem += HandleClick;
		tkItem.OnDownUIItem += OnDown;
		tkItem.OnReleaseUIItem += OnRelease;
	}

		
	/// <summary>
	/// Loads the word selection panel.
	/// </summary>
	IEnumerator LoadWordSelectionPanel()
	{
		
		while (vocalSource.isPlaying)
		{
			yield return null;
		}

		for (int i = 0; i < StaticData.wordsList.Count; i++)
		{
			StartCoroutine( LoadWordButtons (StaticData.wordsList [i], i));

			LoadWordAudioClip ("Words/" + StaticData.wordsList [i]);

			while (vocalSource.isPlaying)
			{
				yield return null;
			}
		}

		LoadWordAudioClip ("Generic/TouchTheWord");

		while (vocalSource.isPlaying)
		{
			yield return null;
		}

		for (int i = 0; i < items.Length; i++)
		{
			items [i].GetComponentInChildren<tk2dTextMesh> ().color = new Color (255f/255f, 76f/255f, 77f/255f, 255f/255f);
		}

		StartCoroutine ("WaitAnyWordAudio");
	}

	/// <summary>
	/// Waits to play "Touch any word" VO after loading Word Selection Panel.
	/// </summary>
	IEnumerator WaitAnyWordAudio()
	{
		while (vocalSource.isPlaying)
		{
			yield return null;
		}

		yield return new WaitForSeconds (5f);

		LoadWordAudioClip ("Generic/TouchAnyWord");
	}

	/// <summary>
	/// Loads the word buttons with text.
	/// </summary>
	IEnumerator LoadWordButtons(string wordName, int btnID)
	{
		GameObject btn = GameObject.Instantiate(Resources.Load("Objects/Prefabs/GenericButton")) as GameObject;
		btn.name = btnID.ToString();
		float hPos = -660 + (660 * btnID);
		btn.transform.position = new Vector3(hPos, -640f, 1f);
		btn.transform.localScale = Vector3.zero;

		btn.GetComponentInChildren<tk2dTextMesh> ().text = wordName.ToUpper();

		btn.GetComponentInChildren<tk2dTextMesh> ().color = new Color (107f/255f, 107f/255f, 107f/255f, 255f/255f);

		Tween btnTween =  btn.transform.DOScale(Vector3.one, 0.25f).SetEase(Ease.OutBack, 3f);
		yield return btnTween.WaitForCompletion ();


		BoxCollider2D btnCol = btn.GetComponent<BoxCollider2D>();
		btnCol.size = btn.GetComponent<MeshRenderer>().bounds.size;

		tk2dUIItem tkItem = btn.GetComponent<tk2dUIItem> ();
		items [btnID] = tkItem;
		tkItem.OnClickUIItem += HandleClick;
		tkItem.OnDownUIItem += OnDown;
		tkItem.OnReleaseUIItem += OnRelease;
	}
		
	/// <summary>
	/// Handles the button press.
	/// </summary>
	void HandleClick(tk2dUIItem clickedItem)
	{
		if (isLoading == true)
			return;

		if (vocalSource.isPlaying)
			return;

		isLoading = true;

		switch (clickedItem.name)
		{
		case "Next":
			StartCoroutine(NextPanel ());
			break;
		case "0":
			WordBtn (clickedItem);
			break;
		case "1":
			WordBtn (clickedItem);
			break;
		case "2":
			WordBtn (clickedItem);
			break;
		}
	}

	/// <summary>
	/// On press release scales up the button
	/// </summary>
	void OnRelease(tk2dUIItem clickedItem)
	{
		DOTween.Kill(clickedItem.transform);
		clickedItem.transform.DOScale(Vector3.one * 1f, 0.25f).SetEase(Ease.OutBack, 5f);
	}

	/// <summary>
	/// On press down scales down the button
	/// </summary>
	void OnDown(tk2dUIItem clickedItem)
	{
		if (vocalSource.isPlaying)
			return;

		DOTween.Kill(clickedItem.transform);
		clickedItem.transform.localScale = Vector3.one * 0.8f;
	}
}
